<?php
	function ajaxpaginate($page, $totalRecords, $recordsperpage, $noofpagelinks)
	{
		$startResults = ($page - 1) * $recordsperpage;
		$noofpages=ceil($totalRecords/$recordsperpage);
		$page=($page>$noofpages || $page < 1)?1:$page;

		if($noofpagelinks % 2 == 0){
			$middlepage=($noofpagelinks/2)+1;
			$noofpagelinks++;
		}else{
			$middlepage=($noofpagelinks+1)/2;
		}
		
		$startpage=($page < $middlepage)?1 : $page-$middlepage+1;
		$endpage=$startpage+$noofpagelinks-1;
		$endpage = ($noofpages < $endpage) ? $noofpages : $endpage;
		$prev = $page - 1;
		$next = $page + 1;
		
		if($page > 1)
			echo '<a href="javascript:ajaxpagination('.$prev.')" style="color:#138808;">Previous</a>&nbsp;&nbsp;';

		for($i=$startpage; $i<=$endpage; $i++) 
		{
			if($i == $page)
				echo '<span>'.$i.'</span>&nbsp;&nbsp;';
			else
				echo '<a href="javascript:ajaxpagination('.$i.')" style="color:#138808;">'.$i.'</a>&nbsp;&nbsp;';
		}
		
		if ($page < $noofpages)
			echo '<a href="javascript:ajaxpagination('.$next.')" style="color:#138808;">Next</a>';
	}
	?>
	

