<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy fl">Copyright &copy; 2014 | <a href="#">Medilink-Global</a> </p>
            <p class="copy fr">Maintained By <a href="#">OdiTek Solutions</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
            <div class="clear"></div>
      </div>
    </div>
  </div>
</footer> 	
<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 
<!-- JS -->
<script src="js/jquery.js" type="text/javascript"></script>
<!--<script src="js/bootstrap.min.js"></script>--> <!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="js/jquery-ui.min.js"></script> <!-- jQuery UI -->
<script src="js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
<script src="js/custom.js"></script> <!-- Custom codes -->
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script src="js/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.onoff.min.js"></script> <!-- Bootstrap Toggle -->
<script type="text/javascript" src="js/jquery.slimscroll.min.js"></script>
<script src="js/jquery.popupoverlay.js"></script>
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script src="js/angular-datepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.min.js"></script>
<script src="js/dirPagination.js" type="text/javascript"></script>
<script src="js/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="controller/productController.js" type="text/javascript"></script>
    <script src="controller/eVoucherController.js" type="text/javascript"></script>
    <script src="controller/dashboardController.js" type="text/javascript"></script>
    <script src="controller/voucherCodeController.js" type="text/javascript"></script>
    <script src="controller/voucherListController.js" type="text/javascript"></script>
    <script src="controller/customerController.js" type="text/javascript"></script>
    <script src="controller/catagoryController.js" type="text/javascript"></script>  
    <script src="controller/productInfoController.js" type="text/javascript"></script> 
    <script src="controller/productMasterController.js" type="text/javascript"></script> 
    <script src="controller/newProductController.js" type="text/javascript"></script>   
    <script src="controller/productDataController.js" type="text/javascript"></script> 
    <script src="controller/productViewController.js" type="text/javascript"></script>
    <script src="controller/supplierController.js" type="text/javascript"></script>
    <script src="controller/GSTController.js" type="text/javascript"></script>
    <script src="controller/merchantController.js" type="text/javascript"></script>
    <script src="controller/orderController.js" type="text/javascript"></script>
<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
</body>

</html>
