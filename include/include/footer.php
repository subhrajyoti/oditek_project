<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy fl">Copyright &copy; 2014 | <a href="#">Medilink-Global</a> </p>
            <p class="copy fr">Maintained By <a href="#">OdiTek Solutions</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
            <div class="clear"></div>
      </div>
    </div>
  </div>
</footer> 	
<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 
<!-- JS -->
<script src="js/jquery.js" type="text/javascript"></script>
<!--<script src="js/bootstrap.min.js"></script>--> <!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="js/jquery-ui.min.js"></script> <!-- jQuery UI -->
<script src="js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
<script src="js/custom.js"></script> <!-- Custom codes -->
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script src="js/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.onoff.min.js"></script> <!-- Bootstrap Toggle -->
<script type="text/javascript" src="js/jquery.slimscroll.min.js"></script>
<script src="js/jquery.popupoverlay.js"></script>
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.min.js"></script>
<script src="js/bootstrap-multiselect.js" type="text/javascript"></script>
<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
</body>

</html>
