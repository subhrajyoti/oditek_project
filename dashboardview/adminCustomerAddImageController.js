var addImage=angular.module('Spesh');
addImage.controller('adminCustomerAddImageController',function($http,$state,$scope,$window,$timeout,$filter,Upload){
    $scope.formDiv=true;
    $scope.checkedValue = [];
    $scope.totalImageArr=[];
    $scope.buttonName="Add";
    var galFlag=false;
    $scope.openGallery=function(){
        if($scope.subcat.value==null || $scope.subcat.value==''){
            alert('Please select the subcategory');
        }else{
           var imageData=$.param({'action':'getGallImage','subcat_id':$scope.subcat.value});
            $http({
                method:'POST',
                url:"php/customerInfo.php",
                data:imageData,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
               // console.log('ressucc',response);
                $scope.formDiv=false;
                $scope.gallery=true;
                //$scope.galComnt=true;
               // $scope.galEDit="Edit";
               // var str_array = response.data[0].image.split(',');
                $scope.galleryDatas=[];
                for(var i=0;i<response.data.length;i++){
                    if(galFlag==true){
                        if(response.data.length==$scope.checkedValue.length){
                            if(response.data[i].image==$scope.checkedValue[i].image){
                                var data={'gallery_id':response.data[i].	gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit","checked":true};
                                $scope.galleryDatas.push(data);
                            }
                        }else{
                            var data={'gallery_id':response.data[i].	gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit"};
                            $scope.galleryDatas.push(data);
                           $scope.checkedValue.forEach(function(o,i){
                                $scope.galleryDatas.forEach(function(g,i){
                                    if(o.image == g.image){
                                        g['checked'] = true;
                                    }
                                })
                            })
                            //console.log('galleryData',$scope.galleryDatas);
                        }
                    }else{
                        var data={'gallery_id':response.data[i].	gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit"};
                        $scope.galleryDatas.push(data);
                    }
                }
            },function errorCallback(response) {
               // console.log('reserr',response);
                if(response.data['msg']==0){
                     alert('There are no images present in gallery under this subcategory');
                     $scope.formDiv=true;
                     $scope.gallery=false;
                     $scope.galleryDatas=[];
                }
            })
        }
    }
    $scope.clearImage=function(){
         $scope.formDiv=true;
         $scope.gallery=false;
    }
    $scope.setComment=function(index){
        $scope.galleryDatas[index].galEDit="Update";
    }
    $scope.editComment=function(galid,index,comnt,gl){
       // console.log('data',index,$scope.galleryDatas[index].galComnt);
       // $scope.galleryDatas[0].galComnt=false;
        if($scope.galleryDatas[index].galEDit=="Edit"){
            $scope.galleryDatas[index].galComnt=false;
           // console.log('read', $scope.galleryDatas);
        }
       if($scope.galleryDatas[index].galEDit=="Update"){
            var cData=$.param({'action':'setComment','gallery_id':galid,'comment':comnt});
            $http({
                method:'POST',
                url:'php/customerInfo.php',
                data:cData,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
                if(response.data['msg']==1){
                   $scope.galleryDatas[index].galComnt=true;
                   $scope.galleryDatas[index].galEDit="Edit";
                }
            },function errorCallback(response){
            })
        }
    }
    $scope.listOfRestaurant=[{
		name:'Select Restaurant',
		value:''
	}]
	$scope.restaurant=$scope.listOfRestaurant[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=restaurant",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listOfRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
    $scope.listOfSubcat=[{
		name:'Select Subcategory',
		value:''
	}]
	$scope.subcat=$scope.listOfSubcat[0];
    $http({
        method:'GET',
        url:"php/customerInfo.php?action=getsubcategory",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function successCallback(response){
       // console.log('res',response.data);
        angular.forEach(response.data,function(obj){
            var data={'name':obj.subcat_name,'value':obj.subcat_id};
			$scope.listOfSubcat.push(data);
		})
    },function errorCallback(response){
        
    })
    $scope.listOfDays=[{
		name:'Select Day',
		value:''
	}]
	$scope.daysFrom = angular.copy($scope.listOfDays[0]);
    $scope.daysTo = angular.copy($scope.listOfDays[0]);
    $http({
		method:'GET',
		url:"php/customerInfo.php?action=day",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
            var data={'name':obj.day_name,'value':obj.day_id};
            $scope.listOfDays.push(data);
            
        })
	},function errorCallback(response) {
	})
    $scope.clickedRow=function(check,index){
        if(check==true){
            $scope.checkedValue.push($scope.galleryDatas[index]);
           // $scope.totalImageArr
        }else{
            var newIndex = $scope.checkedValue.map(function(e) { return e.image; }).indexOf($scope.galleryDatas[index].image);
            if(newIndex !== -1){
                $scope.checkedValue.splice(newIndex,1);
            }
        }
       console.log('check',$scope.checkedValue);
    }
   $scope.mulImage=[];
   $scope.mulImage.push({'image':null,'filename':'','comment':''});
   $scope.addNewImageRow=function(mulImage){
	   mulImage.push({'image':null,'filename':'','comment':''});
	  // console.log('add file',$scope.mulImage);
	   
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	   mulImage.splice(index,1);
	   console.log('file',$scope.mulImage);
   }
   $scope.onFileSelect1 = function(index) {
	     $scope.mulImage[index]['filename']='';
        // if($scope.browser=='Safari'){
             console.log('file',$scope.mulImage);
        // }
   }
   $http({
       method:'GET',
       url:'php/customerInfo.php?action=getGallerySpecialImage',
       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
   }).then(function successCallback(response){
      // console.log('res',response.data);
        if(response.data.length >0){
            $scope.listOfGallerySpecialImage=response.data;
        }
        if(response.data=='null'){
            $scope.listOfGallerySpecialImage=[];
           // console.log('res',$scope.listOfGallery);
        }
   },function errorCallback(response) {
   })
   $scope.validateImages=function(){
	   var flag;
	   if($scope.mulImage.length >0){
		   for(var i=0;i<$scope.mulImage.length;i++){
			   if($scope.mulImage[i]['image']==null && $scope.mulImage[i]['filename']==''){
				   alert('Please select iamge'+(i+1));
				   var flag=false;
				   return;
			   }else{
				   flag=true;
			   }
		  }
		  return flag;
	   }
   }
   $scope.addGalleryImageDetails=function(billdata){
       if(billdata.$valid){
           var flag=true;
           if($scope.restaurant.value=='' || $scope.restaurant.value==null){
               alert('Please select the business name');
           }else if($scope.subcat.value=='' || $scope.subcat.value==null){
               alert('Please select the subcategory value');
           }else if($scope.daysFrom.value==null || $scope.daysFrom.value==''){
               alert('Please select the from day');
           }else if($scope.daysTo.value==null || $scope.daysTo.value==''){
               alert('Please select the to day');
           }else{
               flag=$scope.validateImages();
               if(flag==true){
                   if($scope.buttonName=='Add'){
                       if($scope.checkedValue.length >0){
                           for(var i=0;i<$scope.checkedValue.length;i++){
                               var data={'image':$scope.checkedValue[i].image,'comment':$scope.checkedValue[i].description,'gallery_image':$scope.checkedValue[i].checked};
                               $scope.totalImageArr.push(data);
                           }
                           for(var i=0;i<$scope.mulImage.length;i++){
                                var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false};
                               $scope.totalImageArr.push(data);
                           }
                       }else{
                            for(var i=0;i<$scope.mulImage.length;i++){
                                var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false};
                               $scope.totalImageArr.push(data);
                           }
                       }
                       if($scope.totalImageArr.length>0){
                           var imageString='';
						   var arrImage=[];
						   var imagearr=[];
                           var uploadArrImage=[];
                           if($scope.totalImageArr[0].image !=null || $scope.totalImageArr[0].image !=''){
                                for(var i=0;i<$scope.totalImageArr.length;i++){
                                    if($scope.totalImageArr[i]['image']!=null){
                                        var newmulpath='';
                                        var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                        if(typeof($scope.totalImageArr[i]['image'])=='object'){
										    newmulpath=today+"_"+ $scope.totalImageArr[i]['image'].name;
                                        }else{
                                          newmulpath=$scope.totalImageArr[i]['image'];
                                        }
										//$scope.mulImage[i]['image'].name=newmulpath;
										$scope.totalImageArr[i]['image']=Upload.rename($scope.totalImageArr[i]['image'], newmulpath);
										arrImage.push({'image':$scope.totalImageArr[i]['image']});
										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
                                    }else{
                                        var newmulpath='';
										newmulpath=$scope.totalImageArr[i]['filename'];
										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
                                    }
                                    //console.log('arrtype',(typeof(arrImage[i]['image'])=='object'));
                                }
                                console.log('arrImage',imageString);
                                console.log('Imagearr',imagearr);
                               for(var i=0;i<arrImage.length;i++){
                                   if(typeof(arrImage[i]['image'])=='object'){
                                       var data={'image':arrImage[i]['image']};
                                       uploadArrImage.push(data);
                                   }
                               }
                                if(arrImage.length>0){
                                     $scope.upload=Upload.upload({
                                         url: 'php/uploadAll.php',
                                         method: 'POST',
                                         file: uploadArrImage
                                     }).success(function(data, status, headers, config) {
                                         var datefrom='';
                                         var dateto='';
                                         if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                              var difference=7;
                                         }else{
                                              var difference=Math.abs((parseInt($scope.daysTo.value)%7)-(parseInt($scope.daysFrom.value)%7))+1;
                                         }
                                         if($scope.date1==null || $scope.date1==''){
                                             datefrom='';
                                         }else{
                                             datefrom=$scope.date1;
                                         }
                                         if($scope.date2==null || $scope.date2==''){
                                             dateto='';
                                         }else{
                                             dateto=$scope.date2;
                                         }
                                         var imageData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'add','diff':difference,'date_from':datefrom,'date_to':dateto});
                                        // console.log('sentData',imageData);
                                         $http({
                                             method:'POST',
                                             url:'php/customerInfo.php',
                                             data:imageData,
                                             headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                         }).then(function successCallback(response){
                                             console.log('succe',response.data);
                                             alert(response.data['msg']);
										     $state.go('dashboard.customer.addimage',{}, { reload: true });
                                         },function errorCallback(response) {
                                              console.log('err',response.data);
                                              alert(response.data['msg']);
                                         })
                                         
                                     }).error(function(data,status){
                                     })
                                }
                           }
                       }
                   }//add section end
                   if($scope.buttonName=='Update'){
                       if($scope.checkedValue.length >0){
                           for(var i=0;i<$scope.checkedValue.length;i++){
                               var data={'image':$scope.checkedValue[i].image,'comment':$scope.checkedValue[i].description,'gallery_image':$scope.checkedValue[i].checked};
                               $scope.totalImageArr.push(data);
                           }
                           for(var i=0;i<$scope.mulImage.length;i++){
                               if($scope.mulImage[i]['image']!=null){
                                   var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false};
                                   $scope.totalImageArr.push(data);
                               }else{
                                   var data={'image':$scope.mulImage[i]['filename'],'comment':$scope.mulImage[i].comment,'gallery_image':false};
                                   $scope.totalImageArr.push(data);
                               }
                           }
                       }else{
                            for(var i=0;i<$scope.mulImage.length;i++){
                               if($scope.mulImage[i]['image']!=null){
                                   var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false};
                                   $scope.totalImageArr.push(data);
                               }else{
                                   var data={'image':$scope.mulImage[i]['filename'],'comment':$scope.mulImage[i].comment,'gallery_image':false};
                                   $scope.totalImageArr.push(data);
                               }
                           }
                       }
                       console.log('total image', $scope.totalImageArr);
                       if($scope.totalImageArr.length>0){
                           var imageString='';
						   var arrImage=[];
						   var imagearr=[];
                           var uploadArrImage=[];
                           if($scope.totalImageArr[0].image !=null || $scope.totalImageArr[0].image !=''){
                               for(var i=0;i<$scope.totalImageArr.length;i++){
                                    if($scope.totalImageArr[i]['image']!=null){
                                        var newmulpath='';
                                        var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                        if(typeof($scope.totalImageArr[i]['image'])=='object'){
										    newmulpath=today+"_"+ $scope.totalImageArr[i]['image'].name;
                                        }else{
                                          newmulpath=$scope.totalImageArr[i]['image'];
                                        }
										//$scope.mulImage[i]['image'].name=newmulpath;
										$scope.totalImageArr[i]['image']=Upload.rename($scope.totalImageArr[i]['image'], newmulpath);
										arrImage.push({'image':$scope.totalImageArr[i]['image']});
										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
                                    }else{
                                        var newmulpath='';
										newmulpath=$scope.totalImageArr[i]['filename'];
										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
                                    }
                                    //console.log('arrtype',(typeof(arrImage[i]['image'])=='object'));
                               }
                               for(var i=0;i<arrImage.length;i++){
                                   if(typeof(arrImage[i]['image'])=='object'){
                                       var data={'image':arrImage[i]['image']};
                                       uploadArrImage.push(data);
                                   }
                               }
                               if(uploadArrImage.length>0){
                                   $scope.upload=Upload.upload({
                                       url: 'php/uploadAll.php',
                                       method: 'POST',
                                       file: uploadArrImage
                                   }).success(function(data, status, headers, config) {
                                       var datefrom='';
                                       var dateto='';
                                      if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                              var difference=7;
                                         }else{
                                              var difference=Math.abs((parseInt($scope.daysTo.value)%7)-(parseInt($scope.daysFrom.value)%7))+1;
                                         }
                                      if($scope.date1==null || $scope.date1==''){
                                         datefrom='';
                                      }else{
                                         datefrom=$scope.date1;
                                      }
                                      if($scope.date2==null || $scope.date2==''){
                                         dateto='';
                                      }else{
                                         dateto=$scope.date2;
                                      }
                                      var imageUpdateData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'update','diff':difference,'date_from':datefrom,'date_to':dateto});
                                       $http({
                                             method:'POST',
                                             url:'php/customerInfo.php',
                                             data:imageUpdateData,
                                             headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                         }).then(function successCallback(response){
                                             console.log('succeup',response.data);
                                             alert(response.data['msg']);
										     $state.go('dashboard.customer.addimage',{}, { reload: true });
                                         },function errorCallback(response) {
                                              console.log('errup',response.data);
                                              alert(response.data['msg']);
                                         })
                                   }).error(function(data,status){
                                   })
                               }else{
                                   var datefrom='';
                                   var dateto='';
                                   if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                              var difference=7;
                                         }else{
                                              var difference=Math.abs((parseInt($scope.daysTo.value)%7)-(parseInt($scope.daysFrom.value)%7))+1;
                                         }
                                  if($scope.date1==null || $scope.date1==''){
                                     datefrom='';
                                  }else{
                                     datefrom=$scope.date1;
                                  }
                                  if($scope.date2==null || $scope.date2==''){
                                     dateto='';
                                  }else{
                                     dateto=$scope.date2;
                                  }
                                  var imageUpdateData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'update','diff':difference,'date_from':datefrom,'date_to':dateto});
                                   $http({
                                         method:'POST',
                                         url:'php/customerInfo.php',
                                         data:imageUpdateData,
                                         headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                     }).then(function successCallback(response){
                                         console.log('succeup',response.data);
                                         alert(response.data['msg']);
                                         $state.go('dashboard.customer.addimage',{}, { reload: true });
                                     },function errorCallback(response) {
                                          console.log('errup',response.data);
                                          alert(response.data['msg']);
                                     })
                               }
                               
                           }
                       }
                   }//update section end
               }
           }
       }
   }
   $scope.viewAllSpecialImages=function(subcatid,memberid){
        var viewData=$.param({'action':'getSpecialImage','subcat_id':subcatid,'member_id':memberid});
        $http({
            method:'POST',
            url:'php/customerInfo.php',
            data:viewData,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
            console.log('view',response.data);
            $scope.stepsSpecialModel=[];
            response.data.forEach(function (a) {
                if (!this[a.day_id]) {
                    this[a.day_id] = {day_id:a.day_id, day_name:a.day_name, special:[]};
                     $scope.stepsSpecialModel.push(this[a.day_id]);
                }
                this[a.day_id].special.push({ image: a.image });
            }, Object.create(null));
          //  console.log('output',$scope.stepsSpecialModel);
           /* for(var i=0;i<response.data.length;i++){
                if($scope.stepsSpecialModel.length > 0){
                    for(var j=0;j<$scope.stepsSpecialModel.length;j++){
                        if($scope.stepsSpecialModel[j]['day_id']==response.data[i]['day_id']){
                            var data={'image':response.data[i]['image']};
                             $scope.stepsSpecialModel[j]['special'].push(data);
                        }else{
                            var data={'image':response.data[i]['image']};
                            specImage.push(data);
                            var data1={'day_id':response.data[i]['day_id'],'day_name':response.data[i]['day_name'],'special':specImage};
                            $scope.stepsSpecialModel.push(data1);
                        }
                    }
                }else{
                    var data={'image':response.data[i]['image']};
                    specImage.push(data);
                    var data1={'day_id':response.data[i]['day_id'],'day_name':response.data[i]['day_name'],'special':specImage};
                    $scope.stepsSpecialModel.push(data1);
                }
            }*/
            $scope.showModal = !$scope.showModal;
        },function errorCallback(response) {
        })
   }
   $scope.editSpecialImageData=function(subcatid,memberid){
       var viewData=$.param({'action':'editSpecialGalImage','subcat_id':subcatid,'member_id':memberid});
       $http({
           method:'POST',
           url:'php/customerInfo.php',
           data:viewData,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
          // console.log('edit',response.data);
           $scope.restaurant.value=response.data[0].member_id;
           $scope.subcat.value=response.data[0].subcat_id;
           $scope.daysFrom.value=response.data[0].from_day;
           $scope.daysTo.value=response.data[0].to_day;
           $scope.date1=response.data[0].date_from;
           $scope.date2=response.data[0].date_to;
           var specialImages=[];
           response.data.forEach(function (a) {
                if (!this[a.subcat_id]) {
                    this[a.subcat_id] = {subcat_id:a.subcat_id, special:[]};
                     specialImages.push(this[a.subcat_id]);
                }
                this[a.subcat_id].special.push({ image: a.image,gallery_image:a.gallery_image,comment:a.comment});
            }, Object.create(null));
          // console.log('special',specialImages);
           var outputList=[];
           for(var i = 0; i < specialImages[0]['special'].length; i++){
               console.log('x',x);
               if (!outputList.some(x => x.image === specialImages[0]['special'][i].image)){
               var data={ image: specialImages[0]['special'][i].image,gallery_image:specialImages[0]['special'][i].gallery_image,comment:specialImages[0]['special'][i].comment };
               outputList.push(data);
               }
           }
         // console.log('out',outputList);
          // console.log('tf', $scope.mulImage);
           for(var i=0;i<outputList.length;i++){
               if(i==0){
                   if(outputList[i].gallery_image=='false'){
                       $scope.mulImage[0]['filename']=outputList[i].image;
                       $scope.mulImage[0]['comment']=outputList[i]['comment'];
                   }else{
                       galFlag=true;
                       var data={'image':outputList[i].image,'description':outputList[i]['comment'],'gallery_image':outputList[i]['gallery_image'],'checked':outputList[i]['gallery_image']};
                       $scope.checkedValue.push(data);
                   }
               }else{
                   if(outputList[i].gallery_image=='false'){
                       if($scope.mulImage.length==1 &&  $scope.mulImage[0]['filename']==''){
                           $scope.mulImage[0]['filename']=outputList[i].image;
                           $scope.mulImage[0]['comment']=outputList[i]['comment'];
                       }else{
                            $scope.mulImage.push({'image':null,'filename':outputList[i].image,'comment':outputList[i]['comment']});
                       }
                   }else{
                       galFlag=true;
                       var data={'image':outputList[i].image,'description':outputList[i]['comment'],'gallery_image':outputList[i]['gallery_image'],'checked':outputList[i]['gallery_image']};
                       $scope.checkedValue.push(data);
                   }
               }
           }
           console.log('checkedit',$scope.checkedValue);
           $scope.buttonName="Update";
		   $scope.ClearbuttonName="Cancel";
		  $scope.showCancel=true;
       },function errorCallback(response) {
       })
       
       
   }
   $scope.clearImageData=function(){
	    $state.go('dashboard.customer.addimage',{}, { reload: true });
    }
   $scope.deleteProductImageData=function(subcat_id,member_id,from_day,to_day){
       if(from_day=='1' && to_day=='7'){
           var difference=7;
       }else{
           var difference=Math.abs((parseInt(to_day)%7)-(parseInt(from_day)%7))+1;
       }
       var delData=$.param({'action':'delGalSpecImageData','member_id':member_id,'subcat_id':subcat_id,'from_day':from_day,'to_day':to_day,'diff':difference});
       var mesg=$window.confirm("Are you sure to delete this record ?");
       if(mesg){
           $http({
               method:'POST',
               url:'php/customerInfo.php',
               data:delData,
               headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
           }).then(function successCallback(response){
               console.log('del',response.data);
               alert(response.data['msg']);
               $state.go('dashboard.customer.addimage',{}, { reload: true });
           },function errorCallback(response) {
               alert(response.data['msg']);
               $state.go('dashboard.customer.addimage',{}, { reload: true });
           })
       }
   }
})
