var addMap=angular.module('Spesh');
addMap.controller('adminCustomerMapController',function($http,$state,$scope,$window,$timeout,$filter,Upload,$stateParams,$timeout){
    $scope.latitude=$stateParams.latitude;
    $scope.longitude=$stateParams.longitude;
    $scope.title=$stateParams.title;
    //$scope.restName=$stateParams.restName;
    var desc= $scope.latitude+","+ $scope.longitude;
    console.log($scope.latitude,$scope.longitude);
    var markers = [{"lat":$scope.latitude,"lng":$scope.longitude},{"title": $scope.title,"lat":$scope.latitude,"lng":$scope.longitude,"description":desc}];
   // $window.addEventListener('load', onload, false);
   // $window.onload = function () {
    $timeout(function(){
    var mapOptions = {
    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    google.maps.event.trigger(map, "resize");
    var infoWindow = new google.maps.InfoWindow();
    var lat_lng = new Array();
    var latlngbounds = new google.maps.LatLngBounds();
    for (i = 0; i < markers.length; i++) {
    var data = markers[i]
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    lat_lng.push(myLatlng);
    var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: data.title
    });
    latlngbounds.extend(marker.position);
    (function (marker, data) {
    google.maps.event.addListener(marker, "click", function (e) {
    infoWindow.setContent(data.description);
    infoWindow.open(map, marker);
    });
    })(marker, data);
    }
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);
    zoomChangeBoundsListener = 
    google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
        if (this.getZoom()){
            this.setZoom(12);
        }
        });
        setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);
    },2000);
})