var resource=angular.module('Channabasavashwara');
resource.controller('resourceController',function($scope,$http,$window,$state,focusResourceField){
	$scope.buttonName="Add";
	$scope.listOfCollege=[{
		name:'Select your college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.addResourceData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select the college name');
				focusResourceField.borderColor('colg_name');
			}else if($scope.user_role==null || $scope.user_role==''){
				alert('User role field could not be blank');
				focusResourceField.borderColor('resourcerole');
			}else if($scope.user_type.value==null || $scope.user_type.value==''){
				alert('type field could not be blank');
				focusResourceField.borderColor('resourcetype');
			}else if($scope.class_type.value==null || $scope.class_type.value==''){
				alert('Please select class type');
				focusResourceField.borderColor('resourceclasstype');
			}else if($scope.semester==null || $scope.semester==''){
				alert('Semester field could not be blank');
				focusResourceField.borderColor('resourcesem');
			}else if($scope.section==null || $scope.section==''){
				alert('Section field could not be blank');
				focusResourceField.borderColor('resourcesec');
			}else if($scope.reunit==null || $scope.reunit==''){
				alert('Unit field could not be blank');
				focusResourceField.borderColor('resourceunit');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'user_role':$scope.user_role,'type':$scope.user_type.value,'semester':$scope.semester,'section':$scope.section,'unit':$scope.reunit,'class_type':$scope.class_type.value};
				$http({
					method:'POST',
					url:"php/resource/addResourceData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
				},function errorCallback(response) {
				});
			}
		}
	}
});
resource.factory('focusResourceField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#555555";
				 }
			});
		}
	};
});