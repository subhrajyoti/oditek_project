var dashboard=angular.module('Channabasavashwara');
dashboard.controller('hodMyplanManagementController',function($scope,$http,$state,$window){
	$scope.myPlanManagementHODtabs = {
    1: ($state.current.name === 'hod.myplanManagement.myTimeTable'),
    2: ($state.current.name === 'hod.myplanManagement.myPlan'),
    3: ($state.current.name === 'hod.myplanManagement.myWDS'),
    };
});
