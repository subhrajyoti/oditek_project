var dashboard=angular.module('Channabasavashwara');
dashboard.controller('hodMyTimeTableController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Academic Year',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
		
	$http({
		method: 'GET',
		url: "php/usertimetable/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	
	
	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
      
	  $http({
			method: 'GET',
			url: "php/usertimetable/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		
		$http({
			method: 'GET',
			url: "php/usertimetable/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
			if( $scope.session_name.value != "" )
			{
				
				var dataString = "session_id="+$scope.session_name.value;
				//alert("Data:"+dataString);
				$.ajax({ 
				type: "POST",url: "php/timetable/getUserTimetableData.php" ,data: dataString,cache: false,
				success: function(html)
				{ 
					//alert("Received Data::"+html);
					var dobj=jQuery.parseJSON(html);
					$scope.updateFields(dobj);
				} 
				});
			}
		}
		
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			//$("#data_hide").html(dobj.data_hide);
			//alert("::"+dobj.status)
		}
		
		

		
		
});



