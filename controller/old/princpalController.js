var princpal=angular.module('Channabasavashwara');
princpal.controller('princpalController',function($scope,$http,$state){
	 $http({
		method: 'GET',
		url: "php/princpal/readPrincpalData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//$scope.princpalData=response.data;
		$scope.princpalname=response.data[0].princpal_name;
		$scope.doj=response.data[0].doj;
		$scope.email=response.data[0].emailid;
		$scope.loginname=response.data[0].login_name;
	},function errorCallback(response) {
		
	});
	$scope.addPrincpalData=function(){
		if($scope.princpalname==null){
			alert('Princpal name field could not blank');
		}else if($scope.doj==null){
			alert('DOJ field could not blank');
		}else if($scope.email==null){
			alert('Email field could not blank');
		}else if($scope.loginname==null){
			alert('Login name field could not blank');
		}else if($scope.password==null){
			alert('password field could not blank');
		}else if($scope.password != $scope.repass){
			alert('password field should match');
		}else{
		var userdata={'princpal_name':$scope.princpalname,'doj':$scope.doj,'email':$scope.email,'login_name':$scope.loginname,'password':$scope.password};
		//console.log('userdata',userdata);
		$http({
			method: 'POST',
			url: "php/princpal/addPrincpalData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('profile',response);
			alert(response.data['msg']);
			$scope.princpalname=null;
			$scope.doj=null;
			$scope.email=null;
			$scope.loginname=null;
			$scope.password=null;
			//$scope.princpalData.unshift(response.data);
		},function errorCallback(response) {
			alert(response.data['msg']);
		})
		}
	}
})