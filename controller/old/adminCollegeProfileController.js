var dashboard=angular.module('Channabasavashwara');
dashboard.controller('adminCollegeProfileController',function($scope,$http,$location,$window,$state,focusInputField){	
     //$scope.profileData=[];
	 var ele=$window.document.getElementById('procolgtitle');
	 ele.focus();
	 ele.style.borderColor="#cccccc";
	 $scope.buttonName="Add";
	 $scope.showCancel=false;
	 var id='';							  
	 $http({
		method: 'GET',
		url: "php/profile/readProfileData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.profileData=response.data;
	},function errorCallback(response) {
		
	});
	$scope.addProfileData=function(billdata){
		//console.log('button name',$scope.buttonName);
		if($scope.buttonName=="Add"){
		if(billdata.$valid){
			
		if($scope.colgname == null || $scope.colgname == ''){
			alert('College Title field could not be blank...');
			focusInputField.borderColor('procolgtitle');
		}else if($scope.shortname==null || $scope.shortname== '' ){
			alert('College Subtitle field could not be blank...');
			focusInputField.borderColor('procolgsubtitle');
		}else if($scope.contno==null || $scope.contno=='' ){
			alert('Mobile no field could not be blank...');
			focusInputField.borderColor('procolgcontactno');
		}else if($scope.colgemail==null || $scope.colgemail==''){
			alert('Email address field could not be blank...');
			focusInputField.borderColor('procolgmail');
		}else if($scope.colgcode==null || $scope.colgcode==''){
			alert('College code field could not be blank...');
			focusInputField.borderColor('procolgcode');
		}else if($scope.address==null || $scope.address=='' ){
			alert('Address field could not be blank...');
			focusInputField.borderColor('procolgaddress');
		}else{
		var userdata={'colg_name':$scope.colgname,'colg_shname':$scope.shortname,'address':$scope.address,'cont_no':$scope.contno,'email':$scope.colgemail,'code':$scope.colgcode};
		//console.log('userdata',userdata);
		$http({
			method: 'POST',
			url: "php/profile/addProfileData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('profile',response);
			alert(response.data['msg']);
			$state.go('dashboard.profile',{}, { reload: true });
		},function errorCallback(response) {
			//console.log('error response',response);
			alert(response.data['msg']);
			if($scope.colgname==response.data.colg_name){
				focusInputField.borderColor('procolgtitle');
			}else if($scope.contno==response.data.cont_no){
				focusInputField.borderColor('procolgcontactno');
			}else if($scope.colgemail==response.data.email){
				focusInputField.borderColor('procolgmail');
			}else if($scope.colgcode==response.data.code){
				focusInputField.borderColor('procolgcode');
			}else{
				//$state.go('dashboard.profile',{}, { reload: true });
			}
			//$state.go('dashboard.profile',{}, { reload: true })
		})
		}
		}else{
			if(billdata.mobno.$invalid){
			alert('Please add valid mobile no');
			}
			if(billdata.email.$invalid){
				alert('Please add valid email id');
			}
		}
		}
		
		
		if($scope.buttonName=="Update"){
			console.log('update',id);
			if(billdata.$valid){
				if($scope.colgname == null || $scope.colgname == ''){
			alert('College Title field could not be blank...');
			focusInputField.borderColor('procolgtitle');
		}else if($scope.shortname==null || $scope.shortname== '' ){
			alert('College Subtitle field could not be blank...');
			focusInputField.borderColor('procolgsubtitle');
		}else if($scope.contno==null || $scope.contno=='' ){
			alert('Mobile no field could not be blank...');
			focusInputField.borderColor('procolgcontactno');
		}else if($scope.colgemail==null || $scope.colgemail==''){
			alert('Email address field could not be blank...');
			focusInputField.borderColor('procolgmail');
		}else if($scope.colgcode==null || $scope.colgcode==''){
			alert('College code field could not be blank...');
			focusInputField.borderColor('procolgcode');
		}else if($scope.address==null || $scope.address=='' ){
			alert('Address field could not be blank...');
			focusInputField.borderColor('procolgaddress');
		}else{
					var updatedata={'colg_name':$scope.colgname,'colg_shname':$scope.shortname,'address':$scope.address,'cont_no':$scope.contno,'email':$scope.colgemail,'code':$scope.colgcode,'id':id};
					$http({
						method:'POST',
						url:"php/profile/updateProfileData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						alert(response.data['msg']);
						$state.go('dashboard.profile',{}, { reload: true });
					},function errorCallback(response) {
						alert(response.data['msg']);
						if($scope.colgname==response.data.colg_name){
				focusInputField.borderColor('procolgtitle');
			}else if($scope.contno==response.data.cont_no){
				focusInputField.borderColor('procolgcontactno');
			}else if($scope.colgemail==response.data.email){
				focusInputField.borderColor('procolgmail');
			}else if($scope.colgcode==response.data.code){
				focusInputField.borderColor('procolgcode');
			}else{
				$state.go('dashboard.profile',{}, { reload: true });
			}
					});
				}
			}else{
			alert('This form is not valid');
		}
		}
	}
	
	
	$scope.editProfileData=function(pid){
		id=pid;
		var editid={'id':id};
		$http({
			method:'POST',
			url:"php/profile/editProfileData.php",
			data:editid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colgname=response.data[0].colg_name;
		    $scope.shortname=response.data[0].short_name;
		    $scope.address=response.data[0].address;
		    $scope.contno=response.data[0].cont_no;
		    $scope.colgemail=response.data[0].email;
			$scope.colgcode=response.data[0].code;
			$scope.buttonName="Update";
			$scope.ClearbuttonName="Cancel"
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.deleteProfileData=function(pid){
		var id=pid;
		var editid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this profile');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/profile/deleteProfileData.php",
				data:editid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.profile',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				//$state.go('dashboard.profile',{}, { reload: true });
			});
		}
	}
	
	$scope.clearField=function(id){
		focusInputField.clearBorderColor(id);
	}
	
	$scope.clearProfileData=function(){
		$state.go('dashboard.profile',{}, { reload: true });
	}
	if($scope.colgname=='' || $scope.colgname==null){
		focusInputField.clearBorderColor('procolgtitle');
	}else if($scope.shortname==null || $scope.shortname== ''){
		focusInputField.clearBorderColor('procolgsubtitle');
	}else if($scope.contno==null || $scope.contno==''){
		focusInputField.clearBorderColor('procolgcontactno');
	}else if($scope.colgemail==null || $scope.colgemail==''){
		focusInputField.clearBorderColor('procolgmail');
	}else if($scope.colgcode==null || $scope.colgcode==''){
		focusInputField.clearBorderColor('procolgcode');
	}else{
		focusInputField.clearBorderColor('procolgaddress');
	}
});

dashboard.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});