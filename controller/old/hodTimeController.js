var hodTime=angular.module('Channabasavashwara');
hodTime.controller('hodtimeController',function($state,$scope,$http,$sce){
	$scope.listOfSession=[];
	$http({
		method:'GET',
		url: "php/hodtime/getSession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(ses){
			var data={'name':ses.session,'value':ses.session_id};
			$scope.listOfSession.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.listOfFaculty=[];
	$http({
		method:'GET',
		url:"php/hodtime/getFaculty.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(fac){
			var data={'name':fac.user_name,'value':fac.user_id};
			$scope.listOfFaculty.push(data);
		});
	},function errorCallback(response) {
	});
	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
        $http({
			method: 'GET',
			url: "php/hodtime/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		/*$http({
			method: 'GET',
			url: "php/hodtime/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});*/
		$scope.checkTableData=function(){
			if($scope.session_name.value==null){
				alert('Please select the session')
			}else{
				var userdata={'session':$scope.session_name.value,'faculty_name':$scope.fac_name.value};
				$http({
					method:'POST',
					url:"php/hodtime/getTimeData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					console.log('response',response);
					//$scope.detailsstock=response.data;
					var data=response.data['data'];
					//$("#detailsstockid").html(data);
					content = $sce.trustAsHtml(data);
					$scope.timeTableHtml=content;
				},function errorCallback(response) {
				});
			}
		}
});