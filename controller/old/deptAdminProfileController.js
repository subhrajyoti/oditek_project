var homeprofile=angular.module('Channabasavashwara');
homeprofile.controller('deptAdminProfileController',function($scope,$state,$http,focusDeptField){
	$scope.buttonName="Update";
	$scope.showpass=true;
	$scope.hidebtn=true;
	$http({
		method:'GET',
		url:"php/userprofile/getProfileData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.uname=response.data[0].user_name;
		$scope.emailid=response.data[0].email;
		$scope.mob_no=response.data[0].mob_no;
		$scope.login_name=response.data[0].login_name;
		
	},function errorCallback(response) {
	});
	$scope.clearData=function(){
		$state.go('home.profile',{},{reload:true});
	}
	$scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 $scope.updateProfileData=function(billdata){
		 if(billdata.$valid){
		 if($scope.uname==null || $scope.uname==''){
			 alert('user name field can not be blank');
			  focusDeptField.borderColor('du_name');
		 }else if($scope.emailid=="" || $scope.emailid==null){
			 alert('email field can not be blank');
			  focusDeptField.borderColor('du_email');
		 }else if($scope.mob_no=="" || $scope.mob_no==null){
			 alert('mobile no field can not be blank');
			  focusDeptField.borderColor('du_mob');
		 }else if($scope.login_name==null || $scope.login_name==''){
			 alert('login name field can not be blank'); 
		 }else if($scope.showpass==false && ($scope.password==null || $scope.password=='')){
			 alert('Password field couldnot be blank');
			 focusDeptField.borderColor('passno');
		 }else{
			 var updatedata={'user_name':$scope.uname,'email':$scope.emailid,'mob_no':$scope.mob_no,'password':$scope.password,'login_name':$scope.login_name};
			 //console.log('update',updatedata);
			 $http({
				 method:'POST',
				 url:"php/userprofile/updateProfileData.php",
				 data:updatedata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 //console.log('res',response);
				 alert(response.data['msg']);
				 $state.go('home.profile',{},{reload:true});
			 },function errorCallback(response) {
				 alert(response.data['msg']);
			 })
		 }
		 }else{
			 if(billdata.email.$invalid){
			 alert("Please enter valid email id.");
			 }
			 if(billdata.mobno.$invalid){
				 alert('Please enter valid mobile no.');
			 }
			 if(billdata.pass.$invalid){
				 alert('Please enter valid passsword.')
			 }
		 }
	 }
	 $scope.resetPasswod=function(){
		$scope.hidebtn=false;
		$scope.showpass=false;
		$scope.rejectpass=true;
	}
	$scope.closePass=function(){
		$scope.showpass=true;
		$scope.rejectpass=false;
		$scope.hidebtn=true;
	}
	$scope.clearField=function(id){
		focusDeptField.clearBorderColor(id);
	}
});
homeprofile.factory('focusDeptField',function($timeout, $window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#CCCCCC";
				 }
			});
		}
	};
	
	
});