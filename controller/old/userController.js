var dashboard=angular.module('Channabasavashwara');
dashboard.controller('userController',function($scope,$http,$state,$window){
	 $(window).scrollTop(0);
	 $scope.no_of_stream=0;
	 $scope.no_of_department=0;
	 $scope.no_of_course=0;
	 $scope.no_of_users=0;
	 $scope.principal=0;
	 $scope.hod=0;
	 $scope.tfaculty=0;
	 $scope.listOfFaculty=[];
	$http({
		 method: 'GET',
		 url: 'php/Login/session.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.userTypeName=response.data[0].user_name;
		 $scope.getData(response.data[0].dept_id);
		 $scope.getRoleName(response.data[0].role_id);
		 $scope.getCollegeName(response.data[0].colg_id);
		 //$scope.getAllFaculty(response.data[0].dept_id);
	 },function errorCallback(response) {
		 $state.go('/',{}, { reload: true });
	 });
	 $scope.logout=function(){
		 $http({
		 method: 'POST',
		 url: 'php/Login/logout.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		// console.log('session',response);
		 $state.go('/',{}, { reload: true });
	     //alert(response);
	 },function errorCallback(response) {
		 //console.log('session',response);
		 //alert(response);
		 $state.go('user',{}, { reload: true });
	 });
	 }
	 $scope.getData=function(usertype){
		 userdata={'user_type':usertype};
		 $http({
		 method: 'POST',
		 url: 'php/Login/getDeptData.php',
		 data: userdata,
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.deptName=response.data[0].dept_name
		 
	 },function errorCallback(response) {
		 
	 });
	 }
	 
	 /*
	  $scope.getAllFaculty=function(dept_id){
		 userdata={'dept_id':dept_id};
		 $http({
		 method: 'POST',
		 url: 'php/Login/getDeptFaculty.php',
		 data: userdata,
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.deptName=response.data[0].dept_name
		 
	 },function errorCallback(response) {
		 
	 });
	 }*/
	 
	 
	 $scope.getRoleName=function(roleid){
		 var roleid={'role_id':roleid}
		 $http({
			 method:'POST',
			 url:"php/common/getRoleName.php",
			 data:roleid,
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		 }).then(function successCallback(response){
			 $scope.ownName=response.data[0].role;
		 },function errorCallback(response) {
		 });
	 }
	 $scope.getCollegeName=function(clg_id){
		 //console.log('user type',usertype);
		 userdata={'clg_id':clg_id};
		 $http({
		 method: 'POST',
		 url: 'php/common/getCollegeData.php',
		 data: userdata,
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.collegeName=response.data[0].colg_name;
	 
	 },function errorCallback(response) {
		 
	 });
	 }
	 $http({
		 method: 'GET',
		 url: 'php/college/getUserSummery.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.no_of_stream = response.data['no_stream'];
		 $scope.no_of_course = response.data['no_course'];
		 $scope.no_of_users = response.data['no_user'];
		 $scope.principal = response.data['Puser_name'];
		 $scope.hod=response.data['Huser_name'];
		 $scope.tfaculty=response.data['tfaculty'];
		 angular.forEach(response.data['data'],function(obj){
			//alert("::"+obj.user_name+"::"+obj.role);
			var data={'fname':obj.user_name,'frole':obj.role};
			$scope.listOfFaculty.push(data);
		});
		 
		 //alert("::"+$scope.no_of_college);
	 },function errorCallback(response) {
		//alert("aaaa");
		//$state.go('/',{}, { reload: true }); 
	 });
	 
})