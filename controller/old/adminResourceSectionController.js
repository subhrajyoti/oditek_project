var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceSectionController',function($scope,$http,$state,$window,sectionField){
	$scope.buttonName="Add";
	$scope.sectionData=[];
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.sectionData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSectionData=function(){
		//console.log('section',$scope.sectionData.length);
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college name');
				sectionField.borderColor('colg_name');
			}else if($scope.section==null || $scope.section==''){
				alert('Please add section name...');
				sectionField.borderColor('resourcesection');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'section':$scope.section};
				$http({
					method:'POST',
					url:"php/resource/addSectionData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('response',response,$scope.sectionData,$scope.sectionData.length);
					alert(response.data['msg']);
					$scope.section=null;
					if($scope.sectionData=='null'){
						$scope.sectionData=[];
						$scope.sectionData.push(response.data);
					}else{
						$scope.sectionData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.section==response.data.section_name){
						sectionField.borderColor('resourcesection');
					}else{
					//$state.go('dashboard.res.section',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select your college name');
				sectionField.borderColor('colg_name');
			}else if($scope.section==null || $scope.section==''){
				alert('Please add section name...');
				sectionField.borderColor('resourcesection');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'section':$scope.section,'id':id};
				$http({
					method:'POST',
					url:"php/resource/UpdateSectionData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.section',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.section==response.data.section_name){
						sectionField.borderColor('resourcesection');
					}else{
						//$state.go('dashboard.res.section',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSectionData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editSectionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].clg_id;
			$scope.section=response.data[0].section_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.colgdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSectionData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Section');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/resource/deleteSectionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.res.section',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('dashboard.res.section',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			sectionField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		sectionField.clearBorderColor(id);
	}
	$scope.cancelSectionData=function(){
		$state.go('dashboard.res.section',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('sectionField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});