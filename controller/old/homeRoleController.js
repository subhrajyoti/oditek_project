var dashboard=angular.module('Channabasavashwara');
dashboard.controller('homeroleController',function($scope,$http,$state,$window){
	$scope.buttonName="Add";
	$scope.user_readonly= false;
	$scope.clearButtonName ="Cancel";
	$scope.showCancel = false;
	var id='';
	$scope.listOfDept=[];
	$scope.listOfName=[];
	$scope.listOfRole=[];
	$scope.listOfDept=[{
		name: 'Department Name',
		value: ''
	}]
	$scope.deptName=$scope.listOfDept[0];
	$scope.listOfName=[{
		name: 'User Name',
		value: ''
	}]
	
	$scope.listOfRole=[{
		name: 'Select Role',
		value: ''
	}]
	$scope.user_role = $scope.listOfRole[0];
	
	$http({
		method: 'GET',
		url: "php/homeuserrole/getRole.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var role={'name':obj1.role,'value':obj1.role_id};
			$scope.listOfRole.push(role);
		});
	},function errorCallback(response) {
		
	});
	
	
	
	$http({
		method: 'GET',
		url: "php/homeuserrole/readRoleData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.userHomeRoleValue=response.data;
	},function errorCallback(response) {
	});
	$http({
		method: 'GET',
		url: "php/homeuserrole/getUserData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var user={'name':obj1.user_name,'value':obj1.login_name};
			$scope.listOfName.push(user);
		});
	},function errorCallback(response) {
		
	});
	$scope.user_name = $scope.listOfName[0];
	
	$scope.addHomeUserRoleData=function(){
		if($scope.buttonName=='Add'){
			if($scope.user_name.name==''){
			alert('select user name from list');
		}else if($scope.user_role==null){
			alert('select user role value');
		}else{
			var userdata={'login_name':$scope.user_name.value,'user_role':$scope.user_role.value};
			console.log('role',userdata);
			$http({
				method: 'POST',
				url: "php/homeuserrole/addRoleData.php",
				data: userdata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('succ',response);
				alert(response.data['msg']);
				//$scope.user_name.value='';
				//$scope.user_role=null;
				//$scope.userHomeRoleValue.unshift(response.data);
				$state.go('home.role',{}, { reload: true });
			},function errorCallback(response) {
				//console.log('err',response);
				alert(response.data['msg']);
			});
		}
		}
		if($scope.buttonName=='Update'){
			if($scope.user_name.value==''){
			alert('select user name from list');
		}else if($scope.user_role==null){
			alert('select user role value');
		}else{
			var updatedata={'role_id':$scope.user_role.value,'user_id':id};
			$http({
				method: 'POST',
				url: "php/homeuserrole/updateRoleData.php",
				data: updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data);
				//$scope.user_name.value='';
				//$scope.user_role=null;
				$state.go('home.role',{}, { reload: true })
			},function errorCallback(response) {
				alert(response.data);
			});
		}
		}
	}
	$scope.editHomeUserRoleData=function(rid){
		id=rid;
		var userdata={'userid':id};
		$scope.showCancel = true;
		$scope.user_readonly= true;
		$http({
			method: 'POST',
			url: "php/homeuserrole/editRoleData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			    //console.log('edited data',response.data[0].user_role);
				//alert("aaaa:"+response.data[0].role_id);
				$scope.user_name.value=response.data[0].user_id;
				$scope.user_name.name=response.data[0].user_name;
				$scope.user_role.value=response.data[0].role_id;
			    $scope.buttonName="Update";
			
		},function errorCallback(response) {
		});
	}
	
	 
	  $scope.clearSubjectData=function(){
		   $state.go('home.role',{}, { reload: true });
	 }
	 
	 
	$scope.deleteHomeRoleData=function(rid){
		var deleteUser = $window.confirm('Are you absolutely sure you want to delete?');
		if(deleteUser){
		id=rid;
		var userdata={'userid':id};
		console.log('delete',userdata);
		$http({
			method: 'POST',
			url: "php/homeuserrole/deleteRoleData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data);
			$state.go('home.role',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('home.role',{}, { reload: true });
		});
	}
	}
})