var courseApp=angular.module('Channabasavashwara');
courseApp.controller('coursecontroller',function($scope,$location,$state,$window,$http){
	//console.log('button value',$scope.buttonName);
	//var id=gup( "e_i" );
	$scope.buttonName="Submit";
	var id='';
	$.ajax({
		type:'GET',
		url:"php/readCourseData.php",
		success: function(data){
			$scope.$apply(function(){
				$scope.courseData=angular.fromJson(data);
			});
		}
	})
	$scope.addCourseData=function(){
		//alert($(this).val());
		//console.log('value',$('#addData')[0].defaultValue);
		if($('#addData')[0].defaultValue=='Submit'){
		if($scope.coursename==null){
			alert('course name field could not blank');
		}else if($scope.course_short_name==null){
			alert('short name field could not blank');
		}else if($scope.semester==null){
			alert('semester field could not blank');
		}else{
		var userdata={'course_name':$scope.coursename,'course_short_name':$scope.course_short_name,'semester':$scope.semester};
		console.log('userdata',userdata);
		$.ajax({
			type:'POST',
			url:"php/addCourse.php",
			data:userdata,
			dataType: 'json',
			success: function(response){
				$scope.$apply(function(){
				console.log('returna',response);
				alert(response.msg);
				//alert("Course data added successfully");
				$scope.coursename=null;
				$scope.course_short_name=null;
				$scope.semester=null;
				$scope.courseData.unshift(response);
				});
			},
			error: function(result){
				alert(result.msg);
			}
		})
		}
		}
		if($('#addData')[0].defaultValue=='Update'){
			var userdata={'course_name':$scope.coursename,'course_short_name':$scope.course_short_name,'semester':$scope.semester,'course_id':id};
			console.log('userupdate',userdata);
			$.ajax({
				type:'POST',
				url:"php/updateCourseData.php",
				data:userdata,
				success: function(response){
					$scope.$apply(function(){
						alert(response);
						//$location.reload();
						$scope.coursename=null;
				        $scope.course_short_name=null;
				        $scope.semester=null;
						$state.go('home.course',{}, { reload: true });
					});
				},
				error: function(result){
					alert(result);
				}
			})
			
		}
	}
	/*if(id != ''){
		$scope.buttonName="Update";
		var userdata={'userid':id};
		$.ajax({
			type:'GET',
			url:"php/editCourseData.php",
			data:userdata,
			success: function(data){
				$scope.$apply(function(){
					var result=JSON.parse(data);
					$scope.coursename=result[0].course_name;
					$scope.course_short_name=result[0].short_name;
					$scope.semester=result[0].semester;
				});
			},
			error: function(data){
			}
		})
	}else{
		$scope.buttonName="Submit";
	}*/
	$scope.editCourseData=function(cid){
		id=cid;
		var userdata={'userid':id};
		$.ajax({
			type:'GET',
			url:"php/editCourseData.php",
			data:userdata,
			success: function(data){
				$scope.$apply(function(){
					var result=JSON.parse(data);
					$scope.coursename=result[0].course_name;
					$scope.course_short_name=result[0].short_name;
					$scope.semester=result[0].semester;
					$scope.buttonName="Update";
				});
			},
			error: function(data){
			}
		})
	}
	$scope.deleteCourseData=function(cid){
		id=cid;
		var userdata={'userid':id};
		var deleteUser=$window.confirm('Are you absolutely sure you want to delete?');
		if(deleteUser){
			$http({
			method: 'POST',
			url: "php/deleteCourseData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data);
			$state.go('home.course',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('home.course',{}, { reload: true });
		});
		}
	}
});