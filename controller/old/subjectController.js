var subApp=angular.module('Channabasavashwara');
subApp.controller('subjectcontroller',function($scope,$http,$state,$window){
	$scope.buttonName="Submit";
	$scope.clearButtonName="Cancel";
	$scope.showCancel = false; 
	$scope.showVenue=false;
	var id='';
	$scope.noCourse=[];
	$scope.noCourseTable=[];
	$scope.noSemesters=[];
	
	$scope.noCourse = [{
    name :'Course Name',
    value: ''
    }];
	
	$scope.noSemesters=null;
	
	$scope.noSemesters = [{
    name :'Select Semester',
    value: ''
    }];	
	
	$scope.noVenue=null;
	$scope.noVenue=[];
	$scope.noVenue = [{
    name :'Select Venue',
    value: ''
    }];	
	
$scope.sub_venue=$scope.noVenue[0];
$scope.semester=$scope.noSemesters[0];
$scope.search = $scope.noCourse[0];
$scope.courseName=$scope.noCourse[0];
	//var id=gup( "s_i" );
	$.ajax({
		method: 'GET',
		url: "php/subject/readSubjectData.php",
		success: function(data){
			$scope.$apply(function(){
				$scope.subjectData=angular.fromJson(data);
			});
		},
		error: function(res){
			alert("could not fetch data");
		}
	})
	
	
	$http({
		method: 'GET',
		url: "php/subject/readCourse.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			var Course={'name':obj.course_name , 'value':obj.course_id};
			$scope.noCourse.push(Course);
		});
		
		$scope.noCourseTable = $scope.noCourse;
	},function errorCallback(response) {
		
	});
	
	
	
	$scope.selectedCourse=function(){
		
		$scope.noSemesters=null;
		$scope.noSemesters=[];
		$scope.noSemesters = [{
    	name :'Select Semester',
    	value: ''
    	}];	
		
		var id = $scope.courseName.value;
		//alert("id::"+id);
		var userdata={'course_id':id};
		$http({
			method: 'POST',
			url: "php/subject/readSemester.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var no_semester = obj.semester;
			//alert(""+no_semester);
			var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			for(var i = 0; i < no_semester; i++)
			{
				var val = i+1;
				var roman_val = key[i];
				//alert("::"+val+":::"+roman_val);
				var seme = {'name':roman_val , 'value':val};
				$scope.noSemesters.push(seme);
			}
			//var semester={'name':obj.course_name , 'value':obj.course_id};
			//$scope.noSemesters.push(semester);
			
			});
			
			
			
		},function errorCallback(response) {
		});
	
	}
	
	$scope.selectedCourseTable=function(){
		
		var id = $scope.search.value;
		$scope.noSemestersTable=null;
		$scope.noSemestersTable=[];
		
		//alert("::"+id);
		var userdata={'course_id':id};
		$http({
			method: 'POST',
			url: "php/subject/readSemester.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				//alert(""+no_semester);
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++)
				{
					var val = i+1;
					var roman_val = key[i];
					//alert("::"+val+":::"+roman_val);
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemestersTable.push(seme);
				}
			
			});
		
			},function errorCallback(response) {
		});
		
		
		
	}
	//$scope.course_name=_.chain($scope.subjectData).pluck("course_name").uniq().sortBy().value();
	//$scope.semester=_.chain($scope.subjectData).pluck("semester").uniq().sortBy().value();
	$scope.addSubjectData=function(){
		
		if($scope.buttonName == 'Submit'){
			
			if($scope.showVenue){
				if($scope.courseName.value==''){
				alert("select the course");
			}else if($scope.semester.value==''){
				alert("select the semester");
			}else if($scope.subject_short_name==null){
				alert("short name field cannot be blank");
			}else if($scope.subjectname==null){
				alert("Subject field cannot be blank");
			}else if($scope.sub_type==null){
				alert('select the subject type');
			}else if($scope.sub_venue.value==null){
				alert('select the venue');
			}else{
				var subjectData={'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_short_name':$scope.subject_short_name,'sub_name':$scope.subjectname,'sub_type':$scope.sub_type,'venue':$scope.sub_venue.value};
				alert(""+subjectData);
				//console.log('subject',subjectData)
				$.ajax({
					method: 'POST',
					url: "php/subject/createSubjectData.php",
					data: subjectData,
					dataType: 'json',
					success: function(response){
						$scope.$apply(function(){
						alert(response.msg);
					 	$state.go('home.subject',{}, { reload: true });
						});
					},
					error: function(response){
						alert(response.msg);
					}
				})
			}
			}else{
				if($scope.courseName.value==''){
				alert("Select the course");
			}else if($scope.semester.value==''){
				alert("Select the semester");
			}else if($scope.subjectname==null){
				alert("Subject field cannot be blank");
			}else if($scope.subject_short_name==null){
				alert("Short name field cannot be blank");
			}else if($scope.sub_type==null){
				alert('select the subject type');
			}else{
				var subjectData={'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_short_name':$scope.subject_short_name,'sub_name':$scope.subjectname,'sub_type':$scope.sub_type};
				//console.log('subject',subjectData)
				$.ajax({
					method: 'POST',
					url: "php/subject/createSubjectData.php",
					data: subjectData,
					dataType: 'json',
					success: function(response){
						$scope.$apply(function(){
						alert(response.msg);
						$state.go('home.subject',{}, { reload: true });
						});
					},
					error: function(response){
						alert(response.msg);
					}
				})
			}
			}
			
		}
		if($scope.buttonName=='Update'){
			if($scope.showVenue){
				var subjectData={'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_short_name':$scope.subject_short_name,'sub_name':$scope.subjectname,'sub_type':$scope.sub_type,'sub_id':id,'venue':$scope.sub_venue.value};
			//console.log('userupdate',userdata);
			$.ajax({
				type:'POST',
				url:"php/subject/updateSubjectData.php",
				data:subjectData,
				success: function(response){
					$scope.$apply(function(){
						alert(response);
						$state.go('home.subject',{}, { reload: true });
					});
				},
				error: function(result){
					alert(result);
				}
			})
			}else{
				var subjectData={'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_short_name':$scope.subject_short_name,'sub_name':$scope.subjectname,'sub_type':$scope.sub_type,'sub_id':id};
			//console.log('userupdate',userdata);
			$.ajax({
				type:'POST',
				url:"php/subject/updateSubjectData.php",
				data:subjectData,
				success: function(response){
					$scope.$apply(function(){
						alert(response);
						$state.go('home.subject',{}, { reload: true });
					});
				},
				error: function(result){
					alert(result);
				}
			})
			}
		}
	}
	/*if(id != ''){
		$scope.buttonName="Update";
		var subjectId={'sub_id':id};
		$.ajax({
			method: 'GET',
			url: 'php/subject/editSubjectData.php',
			data: subjectId,
			success:function(response){
				var result=JSON.parse(response);
			    $scope.courseName=result[0].course_name;
			    $scope.semester=result[0].semester;
			    $scope.subject_short_name=result[0].short_name;
			    $scope.subjectname=result[0].subject_name;
			},
			error: function(response){
			}
		})
	}else{
		$scope.buttonName="Submit";
	}*/
	$scope.editSubjectData=function(sid){
		id=sid;
		var subjectId={'sub_id':id};
		$.ajax({
			method: 'GET',
			url: 'php/subject/editSubjectData.php',
			data: subjectId,
			success:function(response){
				var result=JSON.parse(response);
				$scope.showCancel =  true;
				if(result[0].venue!= ''){
					$scope.showVenue=true;
					$scope.courseName.value=result[0].course_id;
			    	$scope.selectedCourse();
					$scope.semester.value =result[0].semester_id;
					$scope.semester.name =result[0].semester;
			      	$scope.subject_short_name=result[0].short_name;
			        $scope.subjectname=result[0].subject_name;
				    $scope.sub_type=result[0].subject_type; 
					$scope.selectVenue();
					$scope.sub_venue.value=result[0].venue;
				}else{
					$scope.showVenue=false;
					$scope.courseName.value=result[0].course_id;
			    	$scope.selectedCourse();
					$scope.semester.value =result[0].semester_id;
					$scope.semester.name =result[0].semester;
			      	$scope.subject_short_name=result[0].short_name;
			        $scope.subjectname=result[0].subject_name;
				    $scope.sub_type=result[0].subject_type;
				}
				$scope.buttonName="Update";
			},
			error: function(response){
			}
		})
	}
	
	$scope.selectVenue=function(){
		
		$scope.noVenue=null;
		$scope.noVenue=[];
		$scope.noVenue = [{
    	name :'Select Venue',
    	value: ''
    	}];	
		
		$http({
		method: 'GET',
		url: "php/subject/readVenue.php",
		}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			var Venue={'name':obj.venue_name , 'value':obj.venue_id};
			$scope.noVenue.push(Venue);
		});
		},function errorCallback(response) {
		
		});
		
		
		if($scope.sub_type=="Lab"){
			$scope.showVenue=true;
		}else{
			$scope.showVenue=false;
		}
		
		
		
		
		
	}
	
	$scope.clearSubjectData=function(){
		$state.go('home.subject',{}, { reload: true });
	}
	
	
	
	$scope.deleteSubjectData=function(sid){
		id=sid;
		var userdata={'userid':id};
		var deleteUser=$window.confirm('Are you sure want to delete Subject?');
		if(deleteUser){
			$http({
			method: 'POST',
			url: "php/subject/deleteSubjectData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data);
			$state.go('home.subject',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('home.subject',{}, { reload: true });
		});
		}
	}
})