var homeprofile=angular.module('Channabasavashwara');
homeprofile.controller('homeprofileController',function($scope,$state,$http){
	$scope.buttonName="Update";
	$http({
		method:'GET',
		url:"php/userprofile/getProfileData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.uname=response.data[0].user_name;
		$scope.emailid=response.data[0].email;
		$scope.mob_no=response.data[0].mob_no;
		$scope.login_name=response.data[0].login_name;
		
	},function errorCallback(response) {
	});
	$scope.clearData=function(){
		$state.go('home.profile',{},{reload:true});
	}
	$scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 $scope.updateProfileData=function(billdata){
		 if(billdata.$valid){
		 if($scope.uname==null){
			 alert('user name field can not be blank');
		 }else if($scope.emailid==""){
			 alert('email field can not be blank');
		 }else if($scope.mob_no==""){
			 alert('mobile no field can not be blank');
		 }else if($scope.login_name==null){
			 alert('login name field can not be blank'); 
		 }else{
			 var updatedata={'user_name':$scope.uname,'email':$scope.emailid,'mob_no':$scope.mob_no,'password':$scope.password,'login_name':$scope.login_name};
			 //console.log('update',updatedata);
			 $http({
				 method:'POST',
				 url:"php/userprofile/updateProfileData.php",
				 data:updatedata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 //console.log('res',response);
				 alert(response.data['msg']);
				 $state.go('home.profile',{},{reload:true});
			 },function errorCallback(response) {
				 alert(response.data['msg']);
			 })
		 }
		 }else{
			 alert("This form is not valid");
		 }
	 }
});