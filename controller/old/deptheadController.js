var dashboard=angular.module('Channabasavashwara');
dashboard.controller('deptheadController',function($scope,$http,$state){
     // var id=gup( "dh_i" );
	 $scope.buttonName="Submit";
	 var id='';
	 $scope.DeptList=[];
	 $http({
		method: 'GET',
		url: "php/depthead/readDeptheadData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.deptHeadData=response.data;
	},function errorCallback(response) {
		
	});
	$scope.DeptList = [{
    name :'select department',
    value: ''
    }];
	$scope.deptlist=$scope.DeptList[0];
	$http({
		method: 'GET',
		url: "php/depthead/readDept.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data, function(obj){
			var Course={'name':obj.dept_name , 'value':obj.dept_name};
			 $scope.DeptList.push(Course);
		});
	},function errorCallback(response) {
		
	});
	$scope.addDeptHeadData=function(){
		if($('#addProfileData')[0].defaultValue=='Submit'){
		if($scope.deptlist==null){
			alert('Department list field could not blank');
		}else if($scope.hod_name==null){
			alert('HOD name field could not blank');
		}else if($scope.email==null){
			alert('Email field could not blank');
		}else if($scope.password==null){
			alert('password field could not blank');
		}else if($scope.password != $scope.repass){
			alert('password could not match');
		}else{
		var userdata={'dept_name':$scope.deptlist.value,'hod_name':$scope.hod_name,'email':$scope.email,'password':$scope.password};
		//console.log('userdata',userdata);
		$http({
			method: 'POST',
			url: "php/depthead/addDeptheadData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('profile',response);
			alert(response.data['msg']);
			$scope.deptlist=null;
			$scope.hod_name=null;
			$scope.email=null;
			$scope.password=null;
			$scope.deptHeadData.unshift(response.data);
		},function errorCallback(response) {
			alert(response.data['msg']);
		})
		}
		}
		if($('#addProfileData')[0].defaultValue=='Update'){
			if($scope.deptlist==null){
			alert('Department list field could not blank');
		}else if($scope.hod_name==null){
			alert('HOD name field could not blank');
		}else if($scope.email==null){
			alert('Email field could not blank');
		}else if($scope.password==null){
			alert('password field could not blank');
		}else if($scope.password != $scope.repass){
			alert('password could not match');
		}else{
			var updatedata={'dept_name':$scope.deptlist.value,'hod_name':$scope.hod_name,'email':$scope.email,'password':$scope.password,'hod_id':id};
			//console.log('userupdate',userdata);
			$http({
			method: 'POST',
			url: "php/depthead/updateDeptheadData.php",
			data: updatedata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data);
			$state.go('dashboard.dept_head',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
		});
		}
		}
	}
	$scope.getDeptData=function(hid){
		id=hid;
		var userdata={'userid':id};
		$http({
			method: 'POST',
			url: "php/depthead/editDeptheadData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			$scope.deptlist=response.data[0].dept_list;
			$scope.hod_name=response.data[0].hod_name;
			$scope.email=response.data[0].emailid;
			$scope.buttonName="Update";
		},function errorCallback(response) {
		});
	}
})
/*function gup( name ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "";
    else
        return results[1];
}*/