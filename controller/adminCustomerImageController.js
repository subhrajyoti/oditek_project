var customerImage=angular.module('Spesh');
customerImage.controller('adminCustomerImageController',function($scope,$state,$http,$window,$timeout,Upload,focusInputImageField){
	$scope.buttonName='Add';
	var memb_id='';
	var da_id='';
	var sub_id='';
    $scope.browser='';
    var browser=focusInputImageField.detectBrowserImageType();
    var os=focusInputImageField.detectOSType();
    if(browser=='Safari' && os=='Windows'){
        $scope.browser='Safari' ;
    }else{
         $scope.browser='';
    }
    console.log('browser',browser,os,$scope.browser);
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=imagedisp",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		if(response.data !='null'){
			$scope.listOfSpecialImages=response.data;
		}
	},function errorCallback(response) {
	})
	$scope.listOfRestaurant=[{
		name:'Select Restaurant',
		value:''
	}]
	$scope.restaurant=$scope.listOfRestaurant[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=restaurant",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listOfRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
	$scope.listOfDays=[{
		name:'Select Days',
		value:''
	}]
	$scope.days=$scope.listOfDays[0];
	$scope.getDayFromSpecial=function(id){
		$scope.listOfDays=[];
		$scope.listOfDays=[{
			name:'Select Days',
			value:''
		}]
		$scope.days=$scope.listOfDays[0];
		var daydata=$.param({'action':'specialdays','res_id':$scope.restaurant.value});
		$http({
		method:'POST',
		url:"php/customerInfo.php",
		data:daydata,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('day res',response.data);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.day_name,'value':obj.dayid};
				$scope.listOfDays.push(data);
			})
		},function errorCallback(response) {
		})
	}
	$scope.listOfSubcat=[{
		name:'Select Subcategory',
		value:''
	}]
	$scope.subcat=$scope.listOfSubcat[0];
	$scope.getSubcategoryValue=function(id){
		$scope.listOfSubcat=[];
		$scope.listOfSubcat=[{
			name:'Select Subcategory',
			value:''
	    }]
	    $scope.subcat=$scope.listOfSubcat[0];
		var subcatdata=$.param({'action':'getsubcatname','member_id':$scope.restaurant.value,'day_id':$scope.days.value});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:subcatdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				//console.log('day res',response.data);
				angular.forEach(response.data,function(obj){
					var data={'name':obj.subcat_name,'value':obj.subid};
					$scope.listOfSubcat.push(data);
				})
			},function errorCallback(response) {
		})
		
	}
   $scope.mulImage=[];
   $scope.mulImage.push({'image':null,'filename':'','comment':''});
   $scope.addNewImageRow=function(mulImage){
	   mulImage.push({'image':null,'filename':'','comment':''});
	  // console.log('add file',$scope.mulImage);
	   
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	   mulImage.splice(index,1);
	   console.log('file',$scope.mulImage);
   }
   $scope.onFileSelect1 = function(index) {
	     $scope.mulImage[index]['filename']='';
        // if($scope.browser=='Safari'){
             console.log('file',$scope.mulImage);
        // }
   }
   $scope.validateImages=function(){
	   var flag;
	   if($scope.mulImage.length >0){
		   for(var i=0;i<$scope.mulImage.length;i++){
			   if($scope.mulImage[i]['image']==null && $scope.mulImage[i]['filename']==''){
				   alert('Please select iamge'+(i+1));
				   var flag=false;
				   return;
			   }else{
				   flag=true;
			   }
		  }
		  return flag;
	   }
   }
   $scope.addImageDetails=function(billdata){
	  // console.log('billdata',billdata.$valid);
	   if(billdata.$valid){
		   var flag=true;
		   if($scope.restaurant.value==null || $scope.restaurant.value==''){
			   alert('Please select Restaurant name');
			   focusInputImageField.borderColor('restau');
		   }else if($scope.days.value==null || $scope.days.value==''){
			   alert('Please select day');
			   focusInputImageField.borderColor('day');
		   }else if($scope.subcat.value==null || $scope.subcat.value==''){
			   alert('Please select sub-category');
			   focusInputImageField.borderColor('subcat');
		   }else{
			   flag=$scope.validateImages();
			   if(flag==true){
				    if($scope.buttonName=='Add'){
				  // console.log('mulImage',$scope.mulImage.length>0);
						if($scope.mulImage.length>0){
						   var imageString='';
						   var arrImage=[];
						   var imagearr=[];
						   if($scope.mulImage[0].image !=null){
							   for(var i=0;i<$scope.mulImage.length;i++){
								   if($scope.mulImage[i]['image']!=null){
										var newmulpath='';
										var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
										newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
										//$scope.mulImage[i]['image'].name=newmulpath;
										$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
										arrImage.push({'image':$scope.mulImage[i]['image']});
										imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
								   }else{
										var newmulpath='';
										newmulpath=$scope.mulImage[i]['filename'];
										imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
								   }
							   }//end of for loop
							   if(arrImage.length>0){
								   $scope.upload=Upload.upload({
										url: 'php/uploadAll.php',
										method: 'POST',
										file: arrImage
								   }).success(function(data, status, headers, config) {
									   var imageData=$.param({'action':'imagesAdd','restaurant':$scope.restaurant.value,'day_id':$scope.days.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'add'});
									  // console.log('mulImage',imageData);
									   $http({
										   method:'POST',
										   url:'php/customerInfo.php',
										   data:imageData,
										   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									   }).then(function successCallback(response){
										   alert(response.data['msg']);
										   $state.go('dashboard.customer.image',{}, { reload: true });
									   },function errorCallback(response) {
											alert(response.data['msg']);
									   })
								   }).error(function(data,status){
								   })//end of error statement
							   }//end of third if
						   }//end of second if statement
					   }//end of first if statement
			        }//Add if end
					if($scope.buttonName=='Update'){
						//console.log('update',$scope.mulImage.length,$scope.mulImage);
						if($scope.mulImage.length>0){
							var imageString='';
						    var arrImage=[];
							var imagearr=[];
							for(var i=0;i<$scope.mulImage.length;i++){
								if($scope.mulImage[i]['image']!=null){
									var newmulpath='';
									var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
									newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
									$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
									arrImage.push({'image':$scope.mulImage[i]['image']});
									imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
									if(i==0){
										imageString=newmulpath;
									}else{
										imageString +=","+newmulpath;
									}
								}else{
									var newmulpath='';
									newmulpath=$scope.mulImage[i]['filename'];
									imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
									if(i==0){
										imageString=newmulpath;
									}else{
										imageString +=","+newmulpath;
									}
								}
							}//end of for loop inside update
							//console.log('arrImage',arrImage);
							if(arrImage.length>0){
								$scope.upload=Upload.upload({
									url: 'php/uploadAll.php',
									method: 'POST',
									file: arrImage
								}).success(function(data, status, headers, config) {
									var imageData=$.param({'action':'imagesAdd','restaurant':$scope.restaurant.value,'day_id':$scope.days.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'update'});
									$http({
										   method:'POST',
										   url:'php/customerInfo.php',
										   data:imageData,
										   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									   }).then(function successCallback(response){
										   alert(response.data['msg']);
										   $state.go('dashboard.customer.image',{}, { reload: true });
									   },function errorCallback(response) {
											alert(response.data['msg']);
									   })
									
								}).error(function(data,status){
								})
							}else{
								var imageData=$.param({'action':'imagesAdd','restaurant':$scope.restaurant.value,'day_id':$scope.days.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'update'});
								//console.log('imgData',imageData);
								$http({
									   method:'POST',
									   url:'php/customerInfo.php',
									   data:imageData,
									   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
								   }).then(function successCallback(response){
									 //  console.log('upres',response.data);
									   alert(response.data['msg']);
									   $state.go('dashboard.customer.image',{}, { reload: true });
								   },function errorCallback(response) {
										alert(response.data['msg']);
								   })
							}
						}//end of first if inside update
					}
			   }
		   }//end of else statement
	   }
   }
   $scope.getDayFromSpecialEdit=function(member_id,dayid){
	    $scope.listOfDays=[];
		$scope.listOfDays=[{
			name:'Select Days',
			value:''
		}]
		$scope.days=$scope.listOfDays[0];
		var daydata=$.param({'action':'specialdays','res_id':member_id});
		$http({
		method:'POST',
		url:"php/customerInfo.php",
		data:daydata,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('day res',response.data);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.day_name,'value':obj.dayid};
				$scope.listOfDays.push(data);
			})
			$scope.days.value=dayid;
		},function errorCallback(response) {
		})
   }
   $scope.getSubcategoryValueEdit=function(member_id,dayid,subcatid){
	    $scope.listOfSubcat=[];
		$scope.listOfSubcat=[{
			name:'Select Subcategory',
			value:''
	    }]
	    $scope.subcat=$scope.listOfSubcat[0];
		var subcatdata=$.param({'action':'getsubcatname','member_id':member_id,'day_id':dayid});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:subcatdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				//console.log('day res',response.data);
				angular.forEach(response.data,function(obj){
					var data={'name':obj.subcat_name,'value':obj.subid};
					$scope.listOfSubcat.push(data);
				})
				$scope.subcat.value=subcatid;
			},function errorCallback(response) {
		})
   }
   $scope.editSpecialImageData=function(memberid,dayid,subcatid){
	   memb_id=memberid;
	   da_id=dayid;
	   sub_id=subcatid;
	   var editImageData=$.param({'action':'editSpecialImage','member_id':memberid,'day_id':dayid,'subcat_id':subcatid});
	   $http({
		   method:'POST',
		   url:"php/customerInfo.php",
		   data:editImageData,
		   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	   }).then(function successCallback(response){
           console.log("edit",response.data);
		   $scope.restaurant.value=response.data.member_id;
		   $scope.getDayFromSpecialEdit(response.data.member_id,response.data.day_id);
		   $scope.getSubcategoryValueEdit(response.data.member_id,response.data.day_id,response.data.subcat_id);
		   var multiImage=response.data.special_image;
		  // var array=multiImage.split(",");
		   for(var i=0;i<multiImage.length;i++){
			   if(i==0){
				 $scope.mulImage[i]['filename']=multiImage[i]['image'];
                 $scope.mulImage[i]['comment']=multiImage[i]['comment'];
			   }
			   if(i !=0){
				 $scope.mulImage.push({'image':null,'filename':multiImage[i]['image'],'comment':multiImage[i]['comment']});
			   }
		   }
		   $scope.buttonName="Update";
		   $scope.ClearbuttonName="Cancel";
		   $scope.showCancel=true;
	   },function errorCallback(response) {
	   })
   }
   $scope.clearImageData=function(){
	    $state.go('dashboard.customer.image',{}, { reload: true });
   }
   $scope.deleteProductImageData=function(memberid,dayid,subcatid){
	   var delImageData=$.param({'action':'delSpecialImage','member_id':memberid,'day_id':dayid,'subcat_id':subcatid});
	   if($window.confirm('Are you want to sure delete this image ?')){
		   $http({
			   method:'POST',
			   url:"php/customerInfo.php",
			   data:delImageData,
			   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		   }).then(function successCallback(response){
			   console.log('res del',response.data);
			   alert(response.data['msg']);
			  $state.go('dashboard.customer.image',{}, { reload: true });
		   },function errorCallback(response) {
			   alert(response.data['msg']);
			   $state.go('dashboard.customer.image',{}, { reload: true });
		   })
	   }
   }
})
customerImage.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});
customerImage.factory('focusInputImageField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		},
        detectBrowserImageType:function(){
            var objbrowserName; 
			 if((navigator.userAgent.indexOf("Edge") != -1 ) || (!!document.documentMode == true )){ //IF IE > 10
              //alert('Edge'); 
              objbrowserName= 'Edge';
            }else if(navigator.userAgent.indexOf("MSIE") != -1 ){
                // alert('IE');
                  objbrowserName='IE';
            } else if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
               // alert('Opera');
                 objbrowserName='Opera';
            }else if(navigator.userAgent.indexOf("Chrome") != -1 ){
               // alert('Chrome');
                objbrowserName='Chrome';
            }else if(navigator.userAgent.indexOf("Safari") != -1){
               // alert('Safari');
                objbrowserName='Safari';
            }else if(navigator.userAgent.indexOf("Firefox") != -1 ) {
                 objbrowserName='Firefox';
            }else{
               //alert('unknown');
            }
            return objbrowserName;
		},
        detectOSType:function(){
            var osName;
            if (navigator.appVersion.indexOf("Win")!=-1){
                osName="Windows";
            }else if(navigator.appVersion.indexOf("Mac")!=-1){
                osName="MacOS";
            }else if(navigator.appVersion.indexOf("X11")!=-1){
                osName="UNIX";
            }else if(navigator.appVersion.indexOf("Linux")!=-1){
                osName="Linux";
            }else{
                
            }
            return osName;
        }
	};
});