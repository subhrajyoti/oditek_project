var custom=angular.module('Spesh');
custom.controller('adminAnalyticsController',function($scope,$http,$state,$window){
	$scope.anatabs = {
    1: ($state.current.name === 'dashboard.analytics.summary'),
    2: ($state.current.name === 'dashboard.analytics.details'),
	3: ($state.current.name === 'dashboard.analytics.subcategorysummary'),
	4: ($state.current.name === 'dashboard.analytics.subcategorydetails'),
    };
});