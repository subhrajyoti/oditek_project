var dashboard=angular.module('Takeme');
dashboard.controller('adminViewRegistrationController',function($scope,$http,$location,$window,$state,$filter,Upload,focusInputField){	
      $scope.ownerlist=true;
	  $scope.ownerlistToEdit = false;
	  $scope.readcolg= true;
	  $scope.buttonName="Save";
	  $scope.listbuttonName="List View";
	  $scope.hidebtn=true;
	  $scope.showpass=true;
	  var owner_id='';
	  var fileURL='';
	  var fileURL1='';
	  var fileURL2='';
	  var fileURL3='';
	  $scope.imageData='';
	  $scope.image2='';
	  $scope.image3='';
	  $scope.image4='';
	  $scope.listGender=[{ name:'Select Gender', value:'0' },{ name:'Male', value:'1' },{ name:'Female', value:'2' }]
	  $scope.gender_type=$scope.listGender[0];
	  
	  $scope.listIndentity=[{ name:'Select Identity Type', value:'0' }, { name:'Driving Licence', value:'1' }, { name:'Pan Card', value:'2'
	}, {  name:'Voter Card', value:'3' } ]
	$scope.identity_type=$scope.listIndentity[0];
	
	$scope.listBikeType=[{ name:'Select Bike Type', value:'0' }, { name:'Geared', value:'1' }, { name:'Non Geared', value:'2' }]
	$scope.bike_type=$scope.listBikeType[0];
	
	 $scope.listServiceType=[{ name:'Select Service Type', value:'0' }, { name:'Self Ride', value:'1' }, { name:'Chauffer Ride', value:'2'
	}, { name:'Both Ride', value:'3'}]
	$scope.service_type=$scope.listServiceType[0];
	 
	$scope.listYesNo=[{ name:'Select', value:'0'},{ name:'ENABLED', value:'1'}, { name:'DISABLED', value:'2'}]
	$scope.enable_service=$scope.listYesNo[0];
	 
	 $http({
		method: 'GET',
		url: "php/registration/readRegistrationData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objUserData=response.data;
	},function errorCallback(response) {
		
	});
	 $scope.resetPasswod=function(){
		$scope.hidebtn=false;
		$scope.showpass=false;
		$scope.rejectpass=true;
	}
	$scope.closePass=function(){
		$scope.showpass=true;
		$scope.rejectpass=false;
		$scope.hidebtn=true;
	}
	 $scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 $scope.viewUserData=function(regid){
		 var userdata={'ownerid':regid};
		 $http({
			 method:'POST',
			 url:"php/registration/getOwnerData.php",
			 data:userdata,
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		 }).then(function successCallback(response){
			 console.log('view data',response.data);
			 $scope.reg_id=response.data[0].reg_id;
			 $scope.ownername=response.data[0].name;
			 $scope.gender_type.value=response.data[0].gender;
			 $scope.mobno=response.data[0].mobile;
			 $scope.email=response.data[0].email;
			 $scope.address=response.data[0].address;
			 $scope.citycode=response.data[0].city;
			 $scope.pincode=response.data[0].pin_code;
			 
			 $scope.identity_type.value=response.data[0].id_proof_type;
			 var element = document.getElementById("identity_type");
			 var value = element.options[$scope.identity_type.value].text;
		 	 $scope.imagetype= value;
			 if($scope.identity_type.value > 0)
				document.getElementById("identitylabel").innerHTML = value+" No :";
			 else
			 {
				document.getElementById("identitylabel").innerHTML = "Identity Proof No :";
			 	$scope.imagetype="Identity Proof";
			 }
			 $scope.iproofno=response.data[0].id_proof_no;
			 $scope.imageData=response.data[0].id_proof_image;
			 $scope.attchImage1="upload/"+response.data[0].id_proof_image;
			 
			 $scope.mcol1=response.data[0].manf_name;
			 $scope.mcol2=response.data[0].model_name;
			 $scope.mcol3=response.data[0].reg_no;
			 $scope.bike_type.value=response.data[0].bike_type;
			 $scope.dopdate=response.data[0].dop;
			 $scope.service_type.value=response.data[0].service_type;
			 $scope.image2=response.data[0].reg_image;
			 $scope.attchImage2="upload/"+response.data[0].reg_image;
			 $scope.image3=response.data[0].bike_image1;
			 $scope.attchImage3="upload/"+response.data[0].bike_image1;
			 $scope.image4=response.data[0].bike_image2;
			 $scope.attchImage4="upload/"+response.data[0].bike_image2;
			 $scope.reg_code = response.data[0].reg_code;
			 $scope.dor = response.data[0].date;
			 $scope.lmo = response.data[0].mod_on;
			 if($scope.lmo.length == 0)
			 	$scope.lmo = "NA";
			 $scope.reg_status = response.data[0].reg_status;
			 $scope.login_status = response.data[0].login_status;
			 $scope.device_type = response.data[0].device_type;
			 $scope.device_id = response.data[0].device_id;
			 $scope.enable_service.value=response.data[0].status;
			 $scope.ownerlist=false;
			 $scope.ownerlistToEdit=true;
			 
			 $scope.owner_info = "Detail of owner " +$scope.ownername+" , Registration Code:"+$scope.reg_code;
			 
			 
			 $scope.showCancel=true;
		 },function errorCallback(response) {
		 });
	}
	 
	 $scope.clearProfileData=function(){
		 $scope.ownerlistToEdit=false;
		 $scope.ownerlist=true;
	 }
	 
	$scope.updateOwnerData=function(billdata,reg_id){
			
		if(billdata.$valid){
			
		if($scope.showpass==false && ($scope.password==null || $scope.password=='')){
			 alert("password field can not be blank");
			 focusInputField.borderColor('passno');
		 }else if($scope.ownername==null || $scope.ownername==''){
				focusInputField.borderColor('colgname1');
				alert('Please add owner name');
			}else if($scope.gender_type.value =='0'){
				focusInputField.borderColor('gender_type');
				alert('Please select gender');
			}else if($scope.mobno==null || $scope.mobno==''){
				focusInputField.borderColor('mobno');
				alert('Please add mobile no');
			}else if($scope.email==null || $scope.email==''){
				focusInputField.borderColor('email');
				alert('Please add email id');
			}else if($scope.address==null || $scope.address==''){
				focusInputField.borderColor('address');
				alert('Please add address');
			}else if($scope.citycode==null || $scope.citycode==''){
				focusInputField.borderColor('citycode');
				alert('Please add City');
			}else if($scope.pincode==null || $scope.pincode==''){
				focusInputField.borderColor('pincode');
				alert('Please add postal pin code');
			}else if($scope.identity_type.value =='0'){
				focusInputField.borderColor('identity_type');
				alert('Please select identity type');
			}else if($scope.imagefile1==null && $scope.imageData == '' ){
				focusInputField.borderColor('imageupload1');
				alert('Please add identity proof image');
			}else if($scope.mcol1==null || $scope.mcol1==''){
				focusInputField.borderColor('mcol1');
				alert('Please add bike manufacture name');
			} else if($scope.mcol2==null || $scope.mcol2==''){
				focusInputField.borderColor('mcol2');
				alert('Please add bike model name');
			}else if($scope.mcol3==null || $scope.mcol3==''){
				focusInputField.borderColor('mcol3');
				alert('Please add bike registration  No');
			}else if($scope.bike_type.value =='0'){
				focusInputField.borderColor('bike_type');
				alert('Please select bike type');
			}else if($scope.dopdate==null || $scope.dopdate==''){
				focusInputField.borderColor('dopdate');
				alert('Please add date of purchase');
			}else if($scope.service_type.value =='0'){
				focusInputField.borderColor('service_type');
				alert('Please select bike service type');
			}else if($scope.imagefile2==null && $scope.image2 == '' ){
				focusInputField.borderColor('imageupload2');
				alert('Please add Bike Registration image');
			}else if($scope.imagefile3==null && $scope.image3 == '' ){
				focusInputField.borderColor('imageupload3');
				alert('Please add first bike image');
			}else if($scope.imagefile4==null && $scope.image4 == '' ){
				focusInputField.borderColor('imageupload4');
				alert('Please add second bike image');
			}else if($scope.enable_service.value == '0' ){
				focusInputField.borderColor('enable_service');
				alert('Please select service status...');
			}else{
				//console.log('images',$scope.imageData,$scope.image2,$scope.image3,$scope.image4);
				  if($scope.imageData!='' &&  $scope.image2 !='' && $scope.image3 !='' && $scope.image4 !=''){
					  var supplierData=$("#billdata").serialize();
					  supplierData+= "&imageData="+$scope.imageData+"&image2="+$scope.image2+"&image3="+$scope.image3+"&image4="+$scope.image4+"&reg_id="+$scope.reg_id;
					  if($scope.showpass==false){
						  supplierData+= "&password="+$scope.password;
					  }
					// console.log('supplier',supplierData);
					  $http({
						  method:'POST',
						  url:'php/registration/updateNewRegistrationData.php',
						  data:supplierData,
						  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					  }).then(function successCallback(response){
						//  console.log('res',response.data);
						  alert(response.data['msg']);
						  $state.go('dashboard.registration.view',{},{reload:true})
					  },function errorCallback(response) {
						  alert(response.data['msg']);
					  })
				  }else{
					  var fileData=[];
					  $scope.firstImage='';
					  $scope.secondImage='';
					  $scope.thirdImage='';
					  $scope.fourthImage='';
					  if($scope.imageData==''){
						  var file=fileURL;
						  $scope.prefix="Identity";
						  $scope.timestamp =$filter('date')(new Date ,'yyyyMMddHHmmss');
						  var a = Math.floor(100000 + Math.random() * 900000);
                          a = a.toString().substring(0, 4);
						  $scope.firstImage=$scope.prefix+"_"+$scope.timestamp+a+"_"+file.name;
						  file.name=$scope.firstImage;
						  var data={'image':file};
						  fileData.push(data);
					  }else{
						  $scope.firstImage=$scope.imageData;
					  }
					  if($scope.image2==''){
						  var file1=fileURL1;
						  $scope.prefix="REG";
						  $scope.timestamp =$filter('date')(new Date ,'yyyyMMddHHmmss');
						  var a = Math.floor(100000 + Math.random() * 900000);
                          a = a.toString().substring(0, 4);
						  $scope.secondImage=$scope.prefix+"_"+$scope.timestamp+a+"_"+file1.name;
						//  file1.name=$scope.secondImage;
						  Upload.rename(file1, $scope.secondImage);
						 // console.log('new path',file1);
						  var data={'image':file1};
						  fileData.push(data);
					  }else{
						   $scope.secondImage=$scope.image2;
					  }
					  if($scope.image3==''){
						  var file2=fileURL2;
						  $scope.prefix="BIKE1";
						  $scope.timestamp =$filter('date')(new Date ,'yyyyMMddHHmmss');
						  var a = Math.floor(100000 + Math.random() * 900000);
						  a = a.toString().substring(0, 4);
						  $scope.thirdImage=$scope.prefix+"_"+$scope.timestamp+a+"_"+file2.name;
						  //file2.name=$scope.thirdImage;
						  Upload.rename(file2, $scope.thirdImage);
						  var data={'image':file2};
						  fileData.push(data);
					  }else{
						  $scope.thirdImage=$scope.image3;
					  }
					  if($scope.image4==''){
						 var file3=fileURL3; 
						 $scope.prefix="BIKE2";
						 $scope.timestamp =$filter('date')(new Date ,'yyyyMMddHHmmss');
						 var a = Math.floor(100000 + Math.random() * 900000);
						 a = a.toString().substring(0, 4);
						 $scope.fourthImage=$scope.prefix+"_"+$scope.timestamp+a+"_"+file3.name;
						// file3.name=$scope.fourthImage;
						 Upload.rename(file3, $scope.fourthImage);
						 var data={'image':file3};
						 fileData.push(data);
					  }else{
						  $scope.fourthImage=$scope.image4;
					  }
					  console.log('file data',fileData);
					  $scope.upload=Upload.upload({
						  url: 'php/registration/upload.php',
						  method:'POST',
						  file: fileData
					  }).success(function(data, status, headers, config) {
						//  console.log('data',data);
						 var supplierData=$("#billdata").serialize();
						 supplierData+= "&imageData="+$scope.firstImage+"&image2="+$scope.secondImage+"&image3="+$scope.thirdImage+"&image4="+$scope.fourthImage+"&reg_id="+$scope.reg_id;
					  if($scope.showpass==false){
						  supplierData+= "&password="+$scope.password;
					  }
					    $http({
						  method:'POST',
						  url:'php/registration/updateNewRegistrationData.php',
						  data:supplierData,
						  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					  }).then(function successCallback(response){
						//  console.log('res',response.data);
						  alert(response.data['msg']);
						  $state.go('dashboard.registration.view',{},{reload:true})
					  },function errorCallback(response) {
						  alert(response.data['msg']);
					  })
					  }).error(function(data, status) {
					  });
				  }
				 
					/*var file1=fileURL1;
					var file2=fileURL2;
					var file3=fileURL3;
					var file4=fileURL4;
					fileData=[{'image':file1},{'image':file2},{'image':file3},{'image':file4}]
					$scope.upload=Upload.upload({
						url: 'php/registration/uploadAll.php',
						method:'POST',
						file: fileData
					}).success(function(data, status, headers, config) {
						var supplierData=$("#billdata").serialize();
						supplierData+= "&imageid="+data+"&reg_id="+reg_id;
						$http({
							method:'POST',
							url:'php/registration/updateegistration.php',
							data:supplierData,
							headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
						}).then(function successCallback(response){
							
							alert(response.data['reg_id']);
							$state.go('dashboard.registration.new',{},{reload:true});
							
						},function errorCallback(response) {
							alert(response.data['msg']);
							if( response.data['type'] == "mobile" )
							{
								focusInputField.borderColor('mobno');
							}
							
						})
						
					}).error(function(data, status) {
						console.log('err file',data);
					})*/
						
			}
		}	
		else{
				if(billdata.mobno.$invalid){
					focusInputField.borderColor('mobno');
					alert('Please add a valid mobile no');
				}
				
				if(billdata.email.$invalid){
					alert('Please add a valid email id');
					focusInputField.borderColor('email');
				}
				
				if(billdata.bannerimage.$invalid){
					alert('Please add proper image(i.e-.png or .jpeg format)');
				}
				
				
				
		}
	
	}
		
	 
	 $scope.showListData=function(){
		  	$scope.ownerlist=true;
	  		$scope.ownerlistToEdit = false;
		 
		 }
	 $scope.checkIdentityType=function(id)
	{
		$scope.clearField(id);
		var element = document.getElementById(id);
		var value = element.options[element.selectedIndex].text;
		 $scope.imagetype= value;
		if(element.selectedIndex > 0)
			document.getElementById("identitylabel").innerHTML = value+" No :";
		else
		{
			document.getElementById("identitylabel").innerHTML = "Identity Proof No :";
			 $scope.imagetype="Identity Proof";
		}
	}
	
	$scope.clearField=function(id){
		focusInputField.clearBorderColor(id);
	}
	 
	 	 //$scope.viewUserData=function(uid){
		/* owner_id=7;
		 var userdata={'ownerid':owner_id};
		 $http({
			method: 'POST',
			url: "php/registration/getOwnerData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
			$scope.colgname1 =response.data[0].name;
			$scope.middle_name =response.data[0].middle_name;
			$scope.last_name =response.data[0].last_name;
			$scope.dob =response.data[0].dob;
			$scope.mobile =response.data[0].mobile;
			$scope.email =response.data[0].email;
			$scope.address =response.data[0].address;
			$scope.city =response.data[0].city;
			$scope.pin_code =response.data[0].pin_code;
			
			if(response.data[0].id_proof_type == 1)
				$scope.identity_proof = "Driving Licence";
			else if(response.data[0].id_proof_type == 2)
				$scope.identity_proof = "Pan Card";
			else if(response.data[0].id_proof_type == 3)
				$scope.identity_proof = "Voter Card";
			$scope.identity_proof_no = response.data[0].id_proof_no;
			$scope.attchImage1 = response.data[0].id_proof_image;
            $scope.readcolg = true;
		   
		},function errorCallback(response) {
		});
	 
	 
	 
	  $scope.editUserData=function(uid){
		
		document.getElementById("viewmultipleitems").style.display = "block";
	 }*/
	  $scope.uploadFile = function(event){
		//console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded = function(e){
    $scope.$apply(function() {
        $scope.attchImage1=e.target.result;
		//$scope.showImage=true;
		focusInputField.clearBorderColor('bannerimage');
    });
	}
	$scope.uploadFile1 = function(event){
		//console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded1; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded1 = function(e){
    $scope.$apply(function() {
        $scope.attchImage2=e.target.result;
		//$scope.showImage1=true;
		focusInputField.clearBorderColor('bannerimage2');
    });
	}
	$scope.uploadFile2 = function(event){
		//console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded2; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded2 = function(e){
    $scope.$apply(function() {
        $scope.attchImage3=e.target.result;
		//$scope.showImage2=true;
		focusInputField.clearBorderColor('bannerimage3');
    });
	}
	$scope.uploadFile3= function(event){
		//console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded3; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded3 = function(e){
    $scope.$apply(function() {
        $scope.attchImage4=e.target.result;
		focusInputField.clearBorderColor('bannerimage4');
    });
	}
	$scope.onFileSelect = function(name,$files) {
		//console.log('select',name,$files);
		switch(name){
			case 'bannerimage':
			   fileURL=$files;
			   $scope.imageData='';
			   break;
			case 'bannerimage2':
			   fileURL1=$files;
			   $scope.image2='';
			  // console.log('select1',$scope.image2);
			   break;
		    case 'bannerimage3':
			   fileURL2=$files;
			   $scope.image3='';
			   break;
			case 'bannerimage4':
			   fileURL3=$files;
			   $scope.image4='';
			   break;
			
		}
	}
	 
	 
	 
}); //end
dashboard.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
dashboard.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});
	 
	 
	 
	 
	 
	
