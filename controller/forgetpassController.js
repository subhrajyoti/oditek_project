var forgetpass=angular.module('Spesh');
forgetpass.controller('forgetpassController',function($http,$state,$scope,$window){
	$scope.user_resetpassword=function(billdata){
		if(billdata.$valid){
			if($scope.mobno==null || $scope.mobno==''){
				alert('Please enter Login Id');
			}else if($scope.email==null || $scope.email==''){
				alert('Please enter email id');
			}else{
				var emailid=$.param({'action':'forgetpass','email':$scope.email,'mob_no':$scope.mobno});
				$http({
					method:'POST',
					url:"php/customerInfo.php",
					data:emailid,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					console.log('res',response.data);
					alert(response.data['msg']);
				},function errorCallback(response) {
					alert(response.data['msg']);
				})
			}
		}else{
			if(billdata.email.$invalid){
				alert('Please enter a valid email id');
			}
		}
	}
})