var dashboard=angular.module('Spesh');
dashboard.controller('adminSubcatCategoryController',function($scope,$http,$location,$window,$state,Upload,focusInputField){	
     //$scope.profileData=[];
	 var ele=$window.document.getElementById('subcatname');
	 ele.focus();
	 ele.style.borderColor="#cccccc";
	 
	 $scope.readcolg = false;
	 $scope.buttonName="Cancel";
	 $scope.cancelButton=false;
	 
	 $scope.submitbuttonName="Add";
	 $scope.submitButton=true;
	 
	 var subcatid='';							  
	
	
	$scope.listcategory=[{ name:'Select Category', value:''}];
	$scope.cat_name=$scope.listcategory[0];
	
	$http({
		method:'GET',
		url:"php/category/getAllCatageoryData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.cat_name,'value':obj.cat_id};
			$scope.listcategory.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.listOfCatName=[];
	$http({
		method:'GET',
		url:"php/category/getAllCatageoryData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.cat_name,'value':obj.cat_id};
			$scope.listOfCatName.push(data);
		});
	},function errorCallback(response) {
	});
	
	
	
	
	$http({
		method: 'GET',
		url: "php/category/getAllSubcatageoryData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objUserData=response.data;
	},function errorCallback(response) {
		
	});
	
	
	
	
	 	
		//////////////////////////////////////////////////////////////////////////////////////////////
		$scope.addSubcategory=function(billdata){
			
		if(billdata.$valid){
			if($scope.submitbuttonName == "Add"){
				
			 if($scope.cat_name==null || $scope.cat_name.value==''){
				focusInputField.borderColor('cat_name');
				alert('Please select category name');
			}else if($scope.theValue==null || $scope.theValue==''){
				alert('Please add order number');
			}else if($scope.subcatname==null || $scope.subcatname==''){
				focusInputField.borderColor('subcatname');
				alert('Please add sub-category name');
			}else{
					var supplierData=$("#billdata").serialize();
						
					$http({
							method:'POST',
							url:'php/category/addSubcategory.php',
							data:supplierData,
							headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
						}).then(function successCallback(response){
							console.log('response',response.data);
							alert(response.data['msg']);
							$state.go('dashboard.category.subcat',{},{reload:true});
							
						},function errorCallback(response) {
							alert(response.data['msg']);
							
						})
				}
			}
			else if($scope.submitbuttonName == "Update"){
				
				if($scope.subcatname==null || $scope.subcatname==''){
					focusInputField.borderColor('subcatname');
					alert('Please add sub-category name');
				}else if($scope.theValue==null || $scope.theValue==''){
                    alert('Please add order number');
                }else if($scope.subcatname==null || $scope.subcatname==''){
                    focusInputField.borderColor('subcatname');
                    alert('Please add sub-category name');
                }else{
						var supplierData=$("#billdata").serialize();
							supplierData +="&subcat_id="+subcatid;
							//alert("::"+supplierData);
						$http({
								method:'POST',
								url:'php/category/updateSubcategory.php',
								data:supplierData,
								headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
							}).then(function successCallback(response){
								
								alert(response.data['msg']);
								$state.go('dashboard.category.subcat',{},{reload:true});
								
							},function errorCallback(response) {
								alert(response.data['msg']);
								
							})
					}
			
			}
			
			
		}	
		else{
			if(billdata.theValue.$invalid){
                alert('This field needs only number(e.g-0,1..9)');
            }		
		}
	
	}
		
	/////////////////////////////////////////////////////////////////////////
	$scope.swapValueToUp=function(index,catid){
		var upid='';
		var upindex=parseInt(index)-1;
		for(var i=0;i<=$scope.objUserData.length;i++){
			if(upindex==i){
				upid=$scope.objUserData[i]['subcat_id'];
			}
		}
		var swapid=$.param({'action':'upsub','currentid':catid,'upid':upid});
		console.log('swapdata',swapid);
		$http({
			method:'POST',
			url:"php/category/swapValueToUp.php",
			data:swapid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
				//console.log('res',response.data);
				$scope.objUserData='';
				$scope.objUserData=response.data;
		},function errorCallback(response) {
		})
	}
	$scope.swapValueToDown=function(index,catid){
		var downid='';
		var upindex=parseInt(index)+1;
		for(var i=0;i<=$scope.objUserData.length;i++){
			if(upindex==i){
				downid=$scope.objUserData[i]['subcat_id'];
			}
		}
		var swapid=$.param({'action':'downsub','currentid':catid,'downid':downid});
		$http({
			method:'POST',
			url:"php/category/swapValueToUp.php",
			data:swapid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
				//console.log('res',response.data);
				$scope.objUserData='';
				$scope.objUserData=response.data;
		},function errorCallback(response) {
		})
	}
	
	
	$scope.editSubcatagoryData =function(subcat_id){
		
		subcatid = subcat_id;
		var supplierData= "subcat_id="+subcat_id;
		$http({
		method:'POST',
		url:'php/category/getSubcatData.php',
		data:supplierData,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('edit res',response.data);
		$scope.cat_name.value = response.data[0].cat_id;
		$scope.subcatname=response.data[0].subcat_name;
		$scope.theValue=parseInt(response.data[0].order_no);
		$scope.cancelButton=true;
	 	$scope.submitbuttonName="Update";
		 $scope.readcolg = true;
		 var ele=$window.document.getElementById('subcattab');
		 ele.focus();
		//alert(response.data['msg']);
		//$state.go('dashboard.category.cat',{},{reload:true});
							
		},function errorCallback(response) {
		alert(response.data['msg']);
		})
	}
	
	
	$scope.removeSubcatagoryData =function(subcat_id){
		if(confirm("Are you sure want to delete sub-category?")){
			subcatid = subcat_id;
			var supplierData= "subcat_id="+subcat_id;
			//alert(supplierData);
			$http({
			method:'POST',
			url:'php/category/deleteSubcategory.php',
			data:supplierData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.category.subcat',{},{reload:true});
			},function errorCallback(response) {
			alert(response.data['msg']);
			})
		
		}
	}
	
	
	
	$scope.cancelUpdate =function(){
		
		$state.go('dashboard.category.subcat',{},{reload:true});
		
	
	}
	
		
	//////////////////////////////////////////////////////////////////////////
	
	$scope.onFileSelect1 = function($files,id) {
		 fileURL1=$files;
		 $scope.imageData1='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile1 = function(event){
    	var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded1; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded1 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage1=e.target.result;
		$scope.attchImage1='';
		$scope.attchImage1=$scope.myImage1;
    });
	}
	
	////////////////////////////////////////////////////////////////////////
	
	$scope.onFileSelect2 = function($files,id) {
		 fileURL2=$files;
		 $scope.imageData2='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile2 = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded2; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded2 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage2=e.target.result;
		$scope.attchImage2='';
		$scope.attchImage2=$scope.myImage2;
    });
	}
	///////////////////////////////////////////////////////////////////////
	
	$scope.onFileSelect3 = function($files,id) {
		 fileURL3=$files;
		 $scope.imageData3='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile3 = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded3; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded3 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage3=e.target.result;
		$scope.attchImage3='';
		$scope.attchImage3=$scope.myImage3;
    });
	}
	
	////////////////////////////////////////////////////////////////////
	
	
	$scope.onFileSelect4 = function($files,id) {
		 fileURL4=$files;
		 $scope.imageData4='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile4 = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded4; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded4 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage4=e.target.result;
		$scope.attchImage4='';
		$scope.attchImage4=$scope.myImage4;
    });
	}
	
	///////////////////////////////////////////////////////////////////////
	
	
	
	$scope.checkBikeType=function(id)
	{
		$scope.clearField(id);
	}


	$scope.checkGenderType=function(id)
	{
		$scope.clearField(id);
	}
	
	$scope.checkServiceType=function(id)
	{
		$scope.clearField(id);
	}
	
	
	$scope.checkIdentityType=function(id)
	{
		$scope.clearField(id);
		var element = document.getElementById(id);
		var value = element.options[element.selectedIndex].text;
		 $scope.imagetype= value;
		if(element.selectedIndex > 0)
			document.getElementById("identitylabel").innerHTML = value+" No :";
		else
		{
			document.getElementById("identitylabel").innerHTML = "Identity Proof No :";
			 $scope.imagetype="Identity Proof";
		}
	}
	
	$scope.checkCATType=function(id)
	{
		$scope.clearField(id);
	}
	
	
	
	
	$scope.editProfileData=function(pid){
		id=pid;
		var editid={'id':id};
		$http({
			method:'POST',
			url:"php/profile/editProfileData.php",
			data:editid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colgname1=response.data[0].colg_name;
		    $scope.shortname=response.data[0].short_name;
		    $scope.address=response.data[0].address;
		    $scope.contno=response.data[0].cont_no;
		    $scope.colgemail=response.data[0].email;
			$scope.colgcode=response.data[0].code;
			$scope.buttonName="Update";
			$scope.ClearbuttonName="Cancel"
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.deleteProfileData=function(pid){
		var id=pid;
		var editid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this profile');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/profile/deleteProfileData.php",
				data:editid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.profile',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				//$state.go('dashboard.profile',{}, { reload: true });
			});
		}
	}
	
	$scope.clearField=function(id){
		focusInputField.clearBorderColor(id);
	}
	
	$scope.clearProfileData=function(){
		$state.go('dashboard.profile',{}, { reload: true });
	}
	if($scope.colgname1=='' || $scope.colgname1==null){
		focusInputField.clearBorderColor('colgmname1');
	}else if($scope.shortname==null || $scope.shortname== ''){
		focusInputField.clearBorderColor('procolgsubtitle');
	}else if($scope.contno==null || $scope.contno==''){
		focusInputField.clearBorderColor('procolgcontactno');
	}else if($scope.colgemail==null || $scope.colgemail==''){
		focusInputField.clearBorderColor('procolgmail');
	}else if($scope.colgcode==null || $scope.colgcode==''){
		focusInputField.clearBorderColor('procolgcode');
	}else{
		focusInputField.clearBorderColor('procolgaddress');
	}
    $scope.inactiveSubcatagoryData=function(subcat_id,status){
        var supplierData=$.param({'subcat_id':subcat_id,'action':'inactive','status':status});
       // console.log('action',supplierData);
        $http({
           method:'POST',
           url:'php/category/actionSubcategory.php',
           data:supplierData,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
            console.log('action res',response.data);
           alert(response.data['msg']);
           $state.go('dashboard.category.subcat',{},{reload:true});
        },function errorCallback(response) {
           alert(response.data['msg']);
        })
    }
});

dashboard.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
dashboard.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});