var business=angular.module('Spesh');
business.controller('businessController',function($state,$scope,$http){
	
	 $scope.no_of_college=0;
	 $scope.no_of_stream=0;
	 $scope.no_of_department=0;
	 $scope.no_of_course=0;
	 $scope.no_of_users=0;
	
	
	 $http({
		 method: 'GET',
		 url: 'php/Login/session.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 //console.log('session',response);
		 $scope.userType=response.data[0].email;
	 },function errorCallback(response) {
		$state.go('/',{}, { reload: true }); 
	 });
	 $scope.logout=function(){
		 $http({
		 method: 'POST',
		 url: 'php/Login/logout.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 //console.log('session',response);
	     //alert(response);
	 },function errorCallback(response) {
		 //console.log('session',response);
		 //alert(response);
	 });
	 }
})