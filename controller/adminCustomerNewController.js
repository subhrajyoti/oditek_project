var customerNew=angular.module('Spesh');
customerNew.controller('adminCustomerNewController',function($http,$state,$scope,$window,$timeout,Upload,focusInputField){
	$scope.buttonName="Save";
	$scope.displaycopy=true;
    var id='';
   // console.log("session exit",$window.sessionStorage.getItem("customerInfo")===null);
    if(window.sessionStorage.getItem("customerInfo")!=null && window.sessionStorage.getItem("isCopyMode")!=null){
          $window.sessionStorage.removeItem("customerInfo");
          $window.sessionStorage.removeItem("isCopyMode");
          $window.sessionStorage.clear();
    }
	var fileURL='';
	$scope.dropdownValue=[];
	$scope.selectedAnswers = [];
	var rowData={};
	var tableIndex=0;
	var arr=[];
	$scope.imageData='';
	$scope.attchImage1="img/no_image.jpg";
	$scope.init = function() {
	$scope.listOfCategory=[];
	$scope.listOfSubCatagory1 = [];
    $scope.setCategoryValue=function(){
    	//console.log('special',$scope.example14model);
       /* if($scope.special.value !='' || $scope.special.value !=null){
            var specdata=$.param({'action':'speccat','speid':$scope.special.value});
            $http({
                method:'POST',
                url:"php/customerInfo.php",
                data:specdata,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
                console.log('res',response.data);
                $scope.listOfCategory=[];
                angular.forEach(response.data,function(obj){
                    var data={'name':obj.cat_name,'value':obj.cat_id};
                    $scope.listOfCategory.push(data);
                })
            },function errorCallback(response) {
            })
        }*/
        if($scope.example14model.length > 0){
            var specdata=$.param({'action':'mulspesc','speid':$scope.example14model});
            $http({
              method:'POST',
              url:'php/customerInfo.php',
              data:specdata,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
              //console.log("multi",response.data);
              $scope.listOfCategory=[];
              angular.forEach(response.data,function(obj){
                  var data={'name':obj.cat_name,'value':obj.cat_id};
                  $scope.listOfCategory.push(data);
              })
            },function errorCallback(response) {
            })
        }
    }
    $scope.onItemSelect=function(){
    	//console.log('property',$scope.example14model);
    	if($scope.example14model.length > 0){
    		//console.log('hii');
    	}
    	/*if($scope.example14model.length > 0){
    		var specdata=$.param({'action':'mulspesc','speid':$scope.example14model});
    		$http({
    			method:'POST',
    			url:'php/customerInfo.php',
    			data:specdata,
    			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    		}).then(function successCallback(response){
    			$scope.listOfCategory=[];
                angular.forEach(response.data,function(obj){
                    var data={'name':obj.cat_name,'value':obj.cat_id};
                    $scope.listOfCategory.push(data);
                })
    		},function errorCallback(response) {
    		})
    	}*/
    }
    $scope.setMultipleSpecial=function(){
     // console.log('datamodel',$scope.example14model)
      var spesArr=[];
      for(var i=0;i<$scope.days.length;i++){
        var specSub=[];
        for(var j=0;j<$scope.days[i].answers.length;j++){
          if($scope.days[i].answers[j].category!=null){
            var data={'value':$scope.days[i].answers[j].subcategory.value};
            specSub.push(data);
          }
        }
        if(specSub.length > 0){
          spesArr.push(specSub);
        }
      }
      //console.log('two',spesArr);
      if($scope.example14model.length > 0){
        var specdata=$.param({'action':'mulspesc','speid':$scope.example14model});
        $http({
          method:'POST',
          url:'php/customerInfo.php',
          data:specdata,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
          //console.log("multi",response.data);
          $scope.listOfCategory=[];
          angular.forEach(response.data,function(obj){
              var data={'name':obj.cat_name,'value':obj.cat_id};
              $scope.listOfCategory.push(data);
          })
          if($scope.listOfCategory.length >0){
            alert("Special Type set successfully");
          }
          /*$timeout(function() {
            var parentLength=$scope.days.length;
            for(var i=0;i<parentLength;i++){
              var childLength=$scope.days[i].answers.length;
              var count=0;
              for(var j=0;j<childLength;j++){
                if(childLength > 1){
                  if(count !=0){
                    if($scope.days[i].answers[j-count].category==null){
                      $scope.days[i].answers.splice(j,1);
                      if(j< (childLength-1)){
                        $scope.listOfSubCategory[i][j]=$scope.listOfSubCategory[i][j+1];
                      }
                      count++;
                    }else{
                      count--;
                    }
                  }else{
                    if($scope.days[i].answers[j].category==null){
                      $scope.days[i].answers.splice(j,1);
                      if(j< (childLength-1)){
                        $scope.listOfSubCategory[i][j]=$scope.listOfSubCategory[i][j+1];
                      }
                      count++;
                    }
                  }
                }else{
                  if($scope.days[i].answers[j].category==null){
                    $scope.days[i].answers[j].category=null;
                    $scope.days[i].answers[j].subcategory=null;
                    $scope.days[i].answers[j].comment='';
                  }else{

                  }
                }
              }
            }
          }, 1000);*/
          $timeout(function() {
             for(var i=0;i<$scope.days.length;i++){
              for (var j = $scope.days[i].answers.length - 1; j >= 0; j--){
                if($scope.days[i].answers.length > 1){
                  if($scope.days[i].answers[j].category==null){
                    $scope.days[i].answers.splice(j,1);
                    //if(j < ($scope.days[i].answers.length-1)){
                      console.log('subcatlist',i,j,$scope.listOfSubCategory[i][j+1]);
                      for(var k=j;k<$scope.days[i].answers.length;k++){
                        $scope.listOfSubCategory[i][k]=$scope.listOfSubCategory[i][k+1];
                      }
                   // }
                  }
                }else{
                  if($scope.days[i].answers[j].category==null){
                    $scope.days[i].answers[j].category=null;
                    $scope.days[i].answers[j].subcategory=null;
                    $scope.days[i].answers[j].comment='';
                  }else{

                  }
                }
              }
             }
          },1000)
        },function errorCallback(response) {
        })
      }
    }
    $scope.setMultiple=function(){
     /* var testArr=[];
      for(var i=0;i<$scope.days.length;i++){
        for(var j=0;j<$scope.days[i].answers.length;j++){
          if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
            if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
              var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
            }else{
              var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
            }
            testArr.push(data);
          }
        }
      }*/
          console.log("test arr",$scope.days);
    }
	$scope.listOfQuadrant=[{
		name:'Select  Quadrant',
		value:''
	}]
	$scope.quadrant=$scope.listOfQuadrant[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=quad",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.quadrant,'value':obj.quad_id};
			$scope.listOfQuadrant.push(data);
		})
	},function errorCallback(response) {
	})
	/*$scope.listOfSpecial=[{
		name:'Select  Special',
		value:''
	}]
	$scope.special=$scope.listOfSpecial[0];*/
	$scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
	$scope.listOfSpecial=[];
	$scope.example14data = [];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=special",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.special_name,'value':obj.special_id};
			var data1={'label':obj.special_name,'id':obj.special_id};
			$scope.listOfSpecial.push(data);
			$scope.example14data.push(data1);
		})
	},function errorCallback(response) {
	})
	$scope.example2settings = {
        displayProp: 'id'
    };
	$scope.listOfCity=[{
		name:'Select  City',
		value:''
	}]
	$scope.citycode=$scope.listOfCity[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=city",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.city_name,'value':obj.city_id};
			$scope.listOfCity.push(data);
		})
	},function errorCallback(response) {
	})
	$scope.days=[];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=day",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('day',response.data);
	angular.forEach(response.data, function(obj) {
      obj.answers = [];
	  obj.chkbox=[];
      $scope.addNewRow(obj.answers);
      $scope.days.push(obj);
	  $scope.selectedAnswers.push(obj);
    });
	},function errorCallback(response) {
	})
	}
	
   $scope.addNewRow = function(answers,hasDelete) {
    answers.push({
      category: null,
      subcategory: null,
      comment: null,
	  //hasDelete: hasDelete ? hasDelete : false
    });
  };
  
  $scope.removeRow = function(answers, index,parent){
	  //console.log('index',$index);
    answers.splice(index, 1);
	$scope.listOfSubCategory[parent].splice(index,1);
    //answers.splice(answers.length-1,1);
  };
  
  $scope.renderWithMode = function(index,catvalue,parent,isEditMode){
    if(isEditMode){
      $scope.removeBorder(index,catvalue,parent);
    }
  };
  $scope.listOfSubCategory = []; 
 // $scope.listOfSubCatagory = [[]]
  $scope.removeBorder=function(index,catvalue,parent,com){
	 //console.log('catvalue',parent,index);
	 // answer_{{$index}}_{{$parent.$index}}_comment
	 // console.log('ind',index,catvalue,parent,document.getElementById('answer_'+index+'_'+parent+'_comment').value);
	 //$scope['listOfSubCatagory'+parent][index]=[];
	//$scope.listOfSubCatagory[index][parent]=[[]];
	//$scope.listOfSubCatagory[index] = []
   // $scope.listOfSubCatagory[index][parent] = []
    //console.log("catvalue",catvalue,catvalue ==undefined);
    if(catvalue !=undefined){
       console.log('catvalue',catvalue);
       var subcategories=[];
    	 var catdata=$.param({'action':'subcat','cat_id':catvalue});
    		$http({
    			method:'POST',
    			url:"php/customerInfo.php",
    			data:catdata,
    			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    		}).then(function successCallback(response){
         // console.log('subcat response',response.data,response.data.length);
    			angular.forEach(response.data,function(obj){
    				var data={'name':obj.subcat_name,'value':obj.subcat_id};
    				if(!$scope.listOfSubCategory[parent]){
                $scope.listOfSubCategory[parent] = [];  
            }
    				//console.log('data',data);
    				subcategories.push(data);
    			})
    			$scope.listOfSubCategory[parent][index]=subcategories;
    		},function errorCallback(response) {
    		}) 
    }else{
      //$scope.listOfSubCategory[parent][index]=[];
    }
  }
  $scope.saveResturantDetails=function(billdata){
	//  console.log('days',$scope.days);
		//console.log('valid',billdata.bannerimage.$error,billdata.$valid);
		if(billdata.$valid){
			if($scope.name==null || $scope.name==''){
				alert('Please add Business Name');
				focusInputField.borderColor('colgname1');
			}/*else if($scope.quadrant.value==null || $scope.quadrant.value==''){
				alert('Please select quadrant value');
				focusInputField.borderColor('quadrant');
			}else if($scope.special.value==null || $scope.special.value==''){
				alert('Please select the special');
				focusInputField.borderColor('special');
			}*/else if($scope.address==null || $scope.address==''){
				alert('Please add address');
				focusInputField.borderColor('address');
			}/*else if($scope.special.value==null || $scope.special.value==''){
				alert('Please select special');$scope.example14model
				focusInputField.borderColor('address');
			}*/else if($scope.example14model.length ==0){
				alert('Please select special');
				focusInputField.borderColor('specials');
			}else if($scope.citycode.value==null || $scope.citycode.value==''){
				alert('Please select city');
				focusInputField.borderColor('citycode');
			}else if($scope.proviance==null || $scope.proviance==''){
				alert('Please add Proviance');
				focusInputField.borderColor('proviance');
			}else if($scope.postal==null || $scope.postal==''){
				alert('Please add postal code');
				focusInputField.borderColor('postal');
			}else if($scope.businessno==null || $scope.businessno==''){
				alert('Please add business no');
				focusInputField.borderColor('businessno');
			}else if($scope.country==null || $scope.country==''){
				alert('Please add the country');
				focusInputField.borderColor('con');
			}else if($scope.latitude==null || $scope.latitude==''){
				alert('please add the latitude');
				focusInputField.borderColor('latitude');
			}else if(isNaN(document.getElementById("latitude").value)){
                alert('Letters are not allowed for latitude.Use only number');
            }else if($scope.longitude==null || $scope.longitude==''){
				alert('Please add the longitude');
				focusInputField.borderColor('longitude');
			}else if(isNaN(document.getElementById("longitude").value)){
                alert('Letters are not allowed for Longitude.Use only number');
            }else if($scope.conperson==null || $scope.conperson==''){
				alert('Please add contact person');
				focusInputField.borderColor('conper');
			}else if($scope.mobno==null || $scope.mobno==''){
				alert('Please add contact number');
				focusInputField.borderColor('mobno');
			}else if($scope.email==null || $scope.email==''){
				alert('Please add the email');
				focusInputField.borderColor('emailid');
			}else if($scope.status==null || $scope.status==''){
				alert('Please select status');
				focusInputField.borderColor('status');
			}else if($scope.premium_service==null || $scope.premium_service==''){
				alert('Please select premium service');
				focusInputField.borderColor('service');
			}/*else if($scope.file==null && $scope.status==1){
				alert('Please select the image');
				focusInputField.borderColor('imagefile1');
			}*/else{
				//console.log('index',$window.sessionStorage.customerInfo,$scope.address);
                //console.log('index',$scope.parentrestname.value);
                var parentBusiness='';
                if($scope.parentrestname==undefined){
                    parentBusiness='';
                }else{
                    parentBusiness=$scope.parentrestname.value;
                }
                 if($window.sessionStorage.getItem("isCopyMode")=='true'){
                      var storage= JSON.parse($window.sessionStorage.customerInfo);
                     if(storage[0].address ==$scope.address && storage[0].city ==$scope.citycode.value &&  storage[0].country == $scope.country && storage[0].proviance == $scope.proviance && storage[0].postal == $scope.postal && storage[0].url == $scope.url && storage[0].person == $scope.conperson && storage[0].email == $scope.email){
                         var supplierData='';
                         if($scope.imageData!=''){
                            var mulImageString='';
                            var arrImage=[];
                            if($scope.mulImage.length>0){
                                for(var i=0;i<$scope.mulImage.length;i++){
                                    if($scope.mulImage[i]['image']!=null){
                                        var newmulpath='';
                                        var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                        newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
                                        //$scope.mulImage[i]['image'].name=newmulpath;
                                        $scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
                                        arrImage.push({'image':$scope.mulImage[i]['image']});
                                        if(i==0){
                                            mulImageString=newmulpath;
                                        }else{
                                            mulImageString +=","+newmulpath;
                                        }
                                    }else{
                                        var newmulpath='';
                                        newmulpath=$scope.mulImage[i]['filename'];
                                        if(i==0){
                                            mulImageString=newmulpath;
                                        }else{
                                            mulImageString +=","+newmulpath;
                                        }
                                    }
                                }
                                if(arrImage.length>0){
                                    console.log('arr iamge',arrImage,$scope.mulImage);
                                    $scope.upload=Upload.upload({
                                         url: 'php/uploadAll.php',
                                         method: 'POST',
                                         file: arrImage
                                    }).success(function(data, status, headers, config) {
                                        supplierData=$("#billdata").serialize();
                                        supplierData+= "&image="+$scope.imageData+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                        $http({
                                        method:'POST',
                                        url:"php/customerInfo.php",
                                        data:supplierData,
                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                    }).then(function successCallback(response){
                                        console.log('res first',response.data);
                                        if(response.data['data']==1){

                                            for(var i=0;i<$scope.days.length;i++){
                                                for(var j=0;j<$scope.days[i].answers.length;j++){
                                                    if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
                                                        if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
                                                            var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
                                                        }else{
                                                            var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
                                                        }
                                                         arr.push(data);
                                                    }
                                                }
                                            }
                                            console.log('arr',arr)

                                            var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
                                            //console.log('detail',detailddata);
                                            $http({
                                                method:'POST',
                                                url:"php/customerInfo.php",
                                                data:detailddata,
                                                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                            }).then(function successCallback(response){
                                                console.log('res',response.data);
                                                alert(response.data['msg']);
                                                $state.go('dashboard.customer.new',{}, { reload: true });
                                            },function errorCallback(response) {
                                            })
                                        }
                                    },function errorCallback(response) {
                                        console.log('err',response.data);
                                        alert(response.data['msg']);
                                    })
                                    }).error(function(data,status){
                                    })
                                }else{
                                    //console.log('arr 1',arrImage,arrImage.length);
                                    supplierData=$("#billdata").serialize();
                                        supplierData+= "&image="+$scope.imageData+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                        // console.log('supplier data1',supplierData);
                                        $http({
                                        method:'POST',
                                        url:"php/customerInfo.php",
                                        data:supplierData,
                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                    }).then(function successCallback(response){
                                        console.log('res first',response.data);
                                        if(response.data['data']==1){

                                            for(var i=0;i<$scope.days.length;i++){
                                                for(var j=0;j<$scope.days[i].answers.length;j++){
                                                    if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
                                                        if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
                                                            var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
                                                        }else{
                                                            var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
                                                        }
                                                         arr.push(data);
                                                    }
                                                }
                                            }

                                            console.log('arr',arr)
                                            var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
                                            //console.log('detail',detailddata);
                                            $http({
                                                method:'POST',
                                                url:"php/customerInfo.php",
                                                data:detailddata,
                                                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                            }).then(function successCallback(response){
                                                console.log('res',response.data);
                                                alert(response.data['msg']);
                                                $state.go('dashboard.customer.new',{}, { reload: true });
                                            },function errorCallback(response) {
                                            })
                                        }
                                    },function errorCallback(response) {
                                        console.log('err',response.data);
                                        alert(response.data['msg']);
                                    })
                                }

                            }

                         }//image data1
                         if($scope.imageData==''){
                                if($scope.file==null){
                                    var newpath='';
                                    var mulImageString='';
                                    var arrImage=[];
                                    if($scope.mulImage.length>0){
                                        for(var i=0;i<$scope.mulImage.length;i++){
                                            if($scope.mulImage[i]['image']!=null){
                                                var newmulpath='';
                                                var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                                newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
                                               // $scope.mulImage[i]['image'].name=newmulpath;
                                               $scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
                                                arrImage.push({'image':$scope.mulImage[i]['image']});
                                                if(i==0){
                                                    mulImageString=newmulpath;
                                                }else{
                                                    mulImageString +=","+newmulpath;
                                                }
                                            }else{
                                                var newmulpath='';
                                                newmulpath=$scope.mulImage[i]['filename'];
                                                if(i==0){
                                                    mulImageString=newmulpath;
                                                }else{
                                                    mulImageString +=","+newmulpath;
                                                }
                                            }
                                        }
                                        if(arrImage.length>0){
                                            $scope.upload=Upload.upload({
                                                url: 'php/uploadAll.php',
                                                method: 'POST',
                                                file: arrImage
                                            }).success(function(data, status, headers, config) {
                                                supplierData=$("#billdata").serialize();
                                                supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                                //console.log('supplier',supplierData);
                                                $http({
                                                    method:'POST',
                                                    url:"php/customerInfo.php",
                                                    data:supplierData,
                                                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                }).then(function successCallback(response){
                                                    console.log('res 2nd',response.data);
                                                    if(response.data['data']==1){
                                                    for(var i=0;i<$scope.days.length;i++){
                                                        for(var j=0;j<$scope.days[i].answers.length;j++){
                                                            if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
                                                                if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
                                                            }else{
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
                                                            }
                                                                 arr.push(data);
                                                            }
                                                        }
                                                    }

                                                    var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});

                                                    $http({
                                                        method:'POST',
                                                        url:"php/customerInfo.php",
                                                        data:detailddata,
                                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                    }).then(function successCallback(response){
                                                        console.log('res',response.data);
                                                        alert(response.data['msg']);
                                                        $state.go('dashboard.customer.new',{}, { reload: true });
                                                    },function errorCallback(response) {
                                                    })
                                                }
                                                },function errorCallback(response) {
                                                    alert(response.data['msg']);
                                                })
                                            }).error(function(data,status){
                                            })
                                        }else{
                                            supplierData=$("#billdata").serialize();
                                            supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                            // console.log('supplier',supplierData);
                                            $http({
                                                    method:'POST',
                                                    url:"php/customerInfo.php",
                                                    data:supplierData,
                                                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                }).then(function successCallback(response){
                                                    console.log('res 2nd',response.data);
                                                    if(response.data['data']==1){
                                                    for(var i=0;i<$scope.days.length;i++){
                                                        for(var j=0;j<$scope.days[i].answers.length;j++){
                                                            if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
                                                                if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
                                                            }else{
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
                                                            }
                                                                 arr.push(data);
                                                            }
                                                        }
                                                    }

                                                    var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});

                                                    $http({
                                                        method:'POST',
                                                        url:"php/customerInfo.php",
                                                        data:detailddata,
                                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                    }).then(function successCallback(response){
                                                        console.log('res',response.data);
                                                        alert(response.data['msg']);
                                                        $state.go('dashboard.customer.new',{}, { reload: true });
                                                    },function errorCallback(response) {
                                                    })
                                                }
                                                },function errorCallback(response) {
                                                    console.log("error",response.data);
                                                    alert(response.data['msg']);
                                                })
                                        }
                                    }

                                }else{
                                    var mulImageString='';
                                    var arrImage=[];
                                    var file=fileURL;
                                    var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                    var newpath=today+"_"+ file.name;
                                    //file.name=newpath;
                                    file = Upload.rename(file, newpath);
                                    $scope.upload = Upload.upload({
                                        url: 'php/upload.php',
                                        method: 'POST',
                                        file: file
                                    }).success(function(data, status, headers, config) {
                                        if($scope.mulImage.length>0){
                                            for(var i=0;i<$scope.mulImage.length;i++){
                                                if($scope.mulImage[i]['image']!=null){
                                                    var newmulpath='';
                                                    var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                                    newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
                                                   // $scope.mulImage[i]['image'].name=newmulpath;
                                                   $scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
                                                    arrImage.push({'image':$scope.mulImage[i]['image']});
                                                    if(i==0){
                                                        mulImageString=newmulpath;
                                                    }else{
                                                        mulImageString +=","+newmulpath;
                                                    }
                                                }else{
                                                    var newmulpath='';
                                                    newmulpath=$scope.mulImage[i]['filename'];
                                                    if(i==0){
                                                        mulImageString=newmulpath;
                                                    }else{
                                                        mulImageString +=","+newmulpath;
                                                    }
                                                }
                                            }
                                            if(arrImage.length>0){
                                                $scope.upload=Upload.upload({
                                                    url: 'php/uploadAll.php',
                                                    method: 'POST',
                                                    file: arrImage
                                                }).success(function(data, status, headers, config) {
                                                    supplierData=$("#billdata").serialize();
                                                    supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                                    $http({
                                                            method:'POST',
                                                            url:"php/customerInfo.php",
                                                            data:supplierData,
                                                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                        }).then(function successCallback(response){
                                                            console.log('res 2nd',response.data);
                                                            if(response.data['data']==1){


                                                                for(var i=0;i<$scope.days.length;i++){
                                                                    for(var j=0;j<$scope.days[i].answers.length;j++){
                                                                        if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
                                                                            if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
                                                            }else{
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
                                                            }
                                                                             arr.push(data);
                                                                        }
                                                                    }
                                                                }

                                                                var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
                                                                $http({
                                                                    method:'POST',
                                                                    url:"php/customerInfo.php",
                                                                    data:detailddata,
                                                                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                                }).then(function successCallback(response){
                                                                    console.log('res',response.data);
                                                                    alert(response.data['msg']);
                                                                    $state.go('dashboard.customer.new',{}, { reload: true });
                                                                },function errorCallback(response) {
                                                                })
                                                            }
                                                        },function errorCallback(response) {
                                                            alert(response.data['msg']);
                                                        })
                                                }).error(function(data,status){
                                                })
                                            }else{
                                                supplierData=$("#billdata").serialize();
                                                supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                                $http({
                                                            method:'POST',
                                                            url:"php/customerInfo.php",
                                                            data:supplierData,
                                                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                        }).then(function successCallback(response){
                                                            console.log('res 2nd',response.data);
                                                            if(response.data['data']==1){


                                                                for(var i=0;i<$scope.days.length;i++){
                                                                    for(var j=0;j<$scope.days[i].answers.length;j++){
                                                                        if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
                                                                            if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
                                                            }else{
                                                                var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
                                                            }
                                                                             arr.push(data);
                                                                        }
                                                                    }
                                                                }

                                                                var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
                                                                $http({
                                                                    method:'POST',
                                                                    url:"php/customerInfo.php",
                                                                    data:detailddata,
                                                                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                                                }).then(function successCallback(response){
                                                                    console.log('res',response.data);
                                                                    alert(response.data['msg']);
                                                                    $state.go('dashboard.customer.new',{}, { reload: true });
                                                                },function errorCallback(response) {
                                                                })
                                                            }
                                                        },function errorCallback(response) {
                                                            alert(response.data['msg']);
                                                        })
                                            }
                                        }

                                    }).error(function(data, status) {
                                    })
                                }
                            }//image data2
                     }else{
                         if($scope.imageData==''){
						if($scope.file !=null && $scope.mulImage.length>0){
							var imageString='';
							var arrImage=[];
							if($scope.mulImage[0].image !=null){
								var file=fileURL;
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								var newpath=today+"_"+ file.name;
								//file.name=newpath;
								file = Upload.rename(file, newpath);
								//console.log('new',file);
								$scope.upload = Upload.upload({
									url: 'php/upload.php',
									method: 'POST',
									file: file
								}).success(function(data, status, headers, config) {
									for(var i=0;i<$scope.mulImage.length;i++){
										//console.log('image',$scope.mulImage[i]['image'].name);
										if($scope.mulImage[i]['image']!=null){
											var newmulpath='';
											var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
											newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
											//$scope.mulImage[i]['image'].name=newmulpath;
											$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
											arrImage.push({'image':$scope.mulImage[i]['image']});
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										//console.log('imagestring',imageString);
										}else{
											var newmulpath='';
											newmulpath=$scope.mulImage[i]['filename'];
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										}
									}
									if(arrImage.length>0){
										$scope.upload=Upload.upload({
											 url: 'php/uploadAll.php',
											 method: 'POST',
											 file: arrImage
										}).success(function(data, status, headers, config) {
											 var supplierData=$("#billdata").serialize();
											 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
											 $http({
												 method:'POST',
												 url:"php/customerInfo.php",
												 data:supplierData,
												 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											 }).then(function successCallback(response){
												 console.log('response',response.data);
												for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												//console.log('detail',detailddata);
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
													console.log('reso',response.data);
													alert(response.data['msg']);
												})
												
												
											 },function errorCallback(response) {
												 console.log('err1',response.data);
												 alert(response.data['msg']);
											 })
										}).error(function(data,status){
										})
									}else{
										var supplierData=$("#billdata").serialize();
											 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
											 $http({
												 method:'POST',
												 url:"php/customerInfo.php",
												 data:supplierData,
												 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											 }).then(function successCallback(response){
												 console.log('response',response.data);
												for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												//console.log('detail',detailddata);
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
													console.log('reso',response.data);
													alert(response.data['msg']);
												})
												
												
											 },function errorCallback(response) {
												 console.log('err1',response.data);
												 alert(response.data['msg']);
											 })
									}
								}).error(function(data, status) {
								})
							}else{
								var file=fileURL;
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								var newpath=today+"_"+ file.name;
								//file.name=newpath;
								file = Upload.rename(file, newpath);
								$scope.upload = Upload.upload({
									url: 'php/upload.php',
									method: 'POST',
									file: file
								}).success(function(data, status, headers, config) {
									var supplierData=$("#billdata").serialize();
										 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										 $http({
											 method:'POST',
											 url:"php/customerInfo.php",
											 data:supplierData,
											 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										 }).then(function successCallback(response){
											 console.log('response',response.data);
											for(var i=0;i<$scope.days.length;i++){
												for(var j=0;j<$scope.days[i].answers.length;j++){
													if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
														if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
														 arr.push(data);
													}
												}
											}
											//console.log('arr',arr);
											var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
											//console.log('detail',detailddata);
											$http({
												method:'POST',
												url:"php/customerInfo.php",
												data:detailddata,
												headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											}).then(function successCallback(response){
												//console.log('res',response.data);
												alert(response.data['msg']);
												$state.go('dashboard.customer.new',{}, { reload: true });
											},function errorCallback(response) {
												console.log('reso',response.data);
												alert(response.data['msg']);
											})
											
											
										 },function errorCallback(response) {
											 console.log('err1',response.data);
											 alert(response.data['msg']);
										 })
								}).error(function(data, status) {
								})
							}
						}
						if($scope.file !=null && $scope.mulImage.length==0){
							var imageString='';
							var file=fileURL;
							var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
							var newpath=today+"_"+ file.name;
							//file.name=newpath;
							file = Upload.rename(file, newpath);
							//console.log('new',file,newpath);
							$scope.upload = Upload.upload({
								url: 'php/upload.php',
								method: 'POST',
								file: file
							}).success(function(data, status, headers, config) {
									 var supplierData=$("#billdata").serialize();
									 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									 $http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									 }).then(function successCallback(response){
										 console.log('response',response.data);
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										//console.log('detail',detailddata);
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
											console.log('reso',response.data);
											alert(response.data['msg']);
										})
										
										
									 },function errorCallback(response) {
										 console.log('err1',response.data);
										 alert(response.data['msg']);
									 })
							}).error(function(data, status) {
							})
						}
						if($scope.file==null && $scope.mulImage.length>0){
							var imageString='';
							var arrImage=[];
							if($scope.mulImage[0].image !=null){
								for(var i=0;i<$scope.mulImage.length;i++){
										if($scope.mulImage[i]['image']!=null){
											var newmulpath='';
											var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
											newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
											//$scope.mulImage[i]['image'].name=newmulpath;
											$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
											arrImage.push({'image':$scope.mulImage[i]['image']});
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										}else{
											var newmulpath='';
											newmulpath=$scope.mulImage[i]['filename'];
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										}
								}
								if(arrImage.length>0){
									$scope.upload=Upload.upload({
										url: 'php/uploadAll.php',
										 method: 'POST',
										 file:arrImage
									}).success(function(data, status, headers, config) {
										var newpath='';
										var supplierData=$("#billdata").serialize();
										supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										$http({
											 method:'POST',
											 url:"php/customerInfo.php",
											 data:supplierData,
											 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
                      console.log('res first',response.data);
											for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
												})
												
										},function errorCallback(response) {
											console.log('err',response.data);
											alert(response.data['msg']);
										})
									}).error(function(data,status){
									})
								}else{
									var newpath='';
										var supplierData=$("#billdata").serialize();
										supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										$http({
											 method:'POST',
											 url:"php/customerInfo.php",
											 data:supplierData,
											 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
                      console.log('res first',response.data);
											for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
												})
												
										},function errorCallback(response) {
											console.log('err',response.data);
											alert(response.data['msg']);
										})
								}
							}else{
									var newpath='';
									var supplierData=$("#billdata").serialize();
									supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									$http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									}).then(function successCallback(response){
                    console.log('res first',response.data);
										for(var i=0;i<$scope.days.length;i++){
												for(var j=0;j<$scope.days[i].answers.length;j++){
													if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
														if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
														 arr.push(data);
													}
												}
											}
											//console.log('arr',arr);
											var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
											$http({
												method:'POST',
												url:"php/customerInfo.php",
												data:detailddata,
												headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											}).then(function successCallback(response){
												//console.log('res',response.data);
												alert(response.data['msg']);
												$state.go('dashboard.customer.new',{}, { reload: true });
											},function errorCallback(response) {
											})
											
									},function errorCallback(response) {
										console.log('err',response.data);
										alert(response.data['msg']);
									})
							}
						}
						if($scope.file==null && $scope.mulImage.length==0){
								var newpath='';
								var imageString='';
								var supplierData=$("#billdata").serialize();
								supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
								$http({
									 method:'POST',
									 url:"php/customerInfo.php",
									 data:supplierData,
									 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
								}).then(function successCallback(response){
                  console.log('respon',response.data);
									for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
										})
										
								},function errorCallback(response) {
									console.log('err',response.data);
									alert(response.data['msg']);
								})
						}
				}
				if($scope.imageData!=''){
					var imageString='';
				    var arrImage=[];
					
						for(var i=0;i<$scope.mulImage.length;i++){
							if($scope.mulImage[i]['image']!=null){
								var newmulpath='';
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
								//$scope.mulImage[i]['image'].name=newmulpath;
								$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
								arrImage.push({'image':$scope.mulImage[i]['image']});
								if(i==0){
									imageString=newmulpath;
								}else{
									imageString +=","+newmulpath;
								}
							}else{
								var newmulpath='';
								newmulpath=$scope.mulImage[i]['filename'];
								if(i==0){
									imageString=newmulpath;
								}else{
									imageString +=","+newmulpath;
								}
						   }
						}
						if(arrImage.length>0){
							//console.log('arr iamge',arrImage);
								$scope.upload=Upload.upload({
									 url: 'php/uploadAll.php',
									 method: 'POST',
									 file: arrImage
								}).success(function(data, status, headers, config) {
									 var supplierData=$("#billdata").serialize();
									 supplierData+= "&image="+$scope.imageData+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									 $http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									 }).then(function successCallback(response){
										 console.log('response',response.data);
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										//console.log('detail',detailddata);
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
											console.log('reso',response.data);
											alert(response.data['msg']);
										})
										
										
									 },function errorCallback(response) {
										 console.log('err1',response.data);
										 alert(response.data['msg']);
									 })
								}).error(function(data,status){
								})
						}else{
							var supplierData=$("#billdata").serialize();
							supplierData+= "&image="+$scope.imageData+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									 $http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									 }).then(function successCallback(response){
										 console.log('response',response.data);
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										//console.log('detail',detailddata);
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
											console.log('reso',response.data);
											alert(response.data['msg']);
										})
										
										
									 },function errorCallback(response) {
										 console.log('err1',response.data);
										 alert(response.data['msg']);
									 })
						}
					
				}
                     }
                 }else{ //first if end
				if($scope.imageData==''){
						if($scope.file !=null && $scope.mulImage.length>0){
							var imageString='';
							var arrImage=[];
							if($scope.mulImage[0].image !=null){
								var file=fileURL;
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								var newpath=today+"_"+ file.name;
								//file.name=newpath;
								file = Upload.rename(file, newpath);
								//console.log('new',file);
								$scope.upload = Upload.upload({
									url: 'php/upload.php',
									method: 'POST',
									file: file
								}).success(function(data, status, headers, config) {
									for(var i=0;i<$scope.mulImage.length;i++){
										//console.log('image',$scope.mulImage[i]['image'].name);
										if($scope.mulImage[i]['image']!=null){
											var newmulpath='';
											var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
											newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
											//$scope.mulImage[i]['image'].name=newmulpath;
											$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
											arrImage.push({'image':$scope.mulImage[i]['image']});
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										//console.log('imagestring',imageString);
										}else{
											var newmulpath='';
											newmulpath=$scope.mulImage[i]['filename'];
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										}
									}
									if(arrImage.length>0){
										$scope.upload=Upload.upload({
											 url: 'php/uploadAll.php',
											 method: 'POST',
											 file: arrImage
										}).success(function(data, status, headers, config) {
											 var supplierData=$("#billdata").serialize();
											 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
											 $http({
												 method:'POST',
												 url:"php/customerInfo.php",
												 data:supplierData,
												 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											 }).then(function successCallback(response){
												 console.log('response',response.data);
												for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												//console.log('detail',detailddata);
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
													console.log('reso',response.data);
													alert(response.data['msg']);
												})
												
												
											 },function errorCallback(response) {
												 console.log('err1',response.data);
												 alert(response.data['msg']);
											 })
										}).error(function(data,status){
										})
									}else{
										var supplierData=$("#billdata").serialize();
											 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
											 $http({
												 method:'POST',
												 url:"php/customerInfo.php",
												 data:supplierData,
												 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											 }).then(function successCallback(response){
												 console.log('response',response.data);
												for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												//console.log('detail',detailddata);
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
													console.log('reso',response.data);
													alert(response.data['msg']);
												})
												
												
											 },function errorCallback(response) {
												 console.log('err1',response.data);
												 alert(response.data['msg']);
											 })
									}
								}).error(function(data, status) {
								})
							}else{
								var file=fileURL;
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								var newpath=today+"_"+ file.name;
								//file.name=newpath;
								file = Upload.rename(file, newpath);
								$scope.upload = Upload.upload({
									url: 'php/upload.php',
									method: 'POST',
									file: file
								}).success(function(data, status, headers, config) {
									var supplierData=$("#billdata").serialize();
										 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										 $http({
											 method:'POST',
											 url:"php/customerInfo.php",
											 data:supplierData,
											 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										 }).then(function successCallback(response){
											 console.log('response',response.data);
											for(var i=0;i<$scope.days.length;i++){
												for(var j=0;j<$scope.days[i].answers.length;j++){
													if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
														if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
														 arr.push(data);
													}
												}
											}
											//console.log('arr',arr);
											var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
											//console.log('detail',detailddata);
											$http({
												method:'POST',
												url:"php/customerInfo.php",
												data:detailddata,
												headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											}).then(function successCallback(response){
												//console.log('res',response.data);
												alert(response.data['msg']);
												$state.go('dashboard.customer.new',{}, { reload: true });
											},function errorCallback(response) {
												console.log('reso',response.data);
												alert(response.data['msg']);
											})
											
											
										 },function errorCallback(response) {
											 console.log('err1',response.data);
											 alert(response.data['msg']);
										 })
								}).error(function(data, status) {
								})
							}
						}
						if($scope.file !=null && $scope.mulImage.length==0){
							var imageString='';
							var file=fileURL;
							var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
							var newpath=today+"_"+ file.name;
							//file.name=newpath;
							file = Upload.rename(file, newpath);
							//console.log('new',file,newpath);
							$scope.upload = Upload.upload({
								url: 'php/upload.php',
								method: 'POST',
								file: file
							}).success(function(data, status, headers, config) {
									 var supplierData=$("#billdata").serialize();
									 supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									 $http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									 }).then(function successCallback(response){
										 console.log('response',response.data);
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										//console.log('detail',detailddata);
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
											console.log('reso',response.data);
											alert(response.data['msg']);
										})
										
										
									 },function errorCallback(response) {
										 console.log('err1',response.data);
										 alert(response.data['msg']);
									 })
							}).error(function(data, status) {
							})
						}
						if($scope.file==null && $scope.mulImage.length>0){
							var imageString='';
							var arrImage=[];
							if($scope.mulImage[0].image !=null){
								for(var i=0;i<$scope.mulImage.length;i++){
										if($scope.mulImage[i]['image']!=null){
											var newmulpath='';
											var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
											newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
											//$scope.mulImage[i]['image'].name=newmulpath;
											$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
											arrImage.push({'image':$scope.mulImage[i]['image']});
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										}else{
											var newmulpath='';
											newmulpath=$scope.mulImage[i]['filename'];
											if(i==0){
												imageString=newmulpath;
											}else{
												imageString +=","+newmulpath;
											}
										}
								}
								if(arrImage.length>0){
									$scope.upload=Upload.upload({
										url: 'php/uploadAll.php',
										 method: 'POST',
										 file:arrImage
									}).success(function(data, status, headers, config) {
										var newpath='';
										var supplierData=$("#billdata").serialize();
										supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										$http({
											 method:'POST',
											 url:"php/customerInfo.php",
											 data:supplierData,
											 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
                      console.log('res first',response.data);
											for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
												})
												
										},function errorCallback(response) {
											console.log('err',response.data);
											alert(response.data['msg']);
										})
									}).error(function(data,status){
									})
								}else{
									var newpath='';
										var supplierData=$("#billdata").serialize();
										supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										$http({
											 method:'POST',
											 url:"php/customerInfo.php",
											 data:supplierData,
											 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
                      console.log('res first',response.data);
											for(var i=0;i<$scope.days.length;i++){
													for(var j=0;j<$scope.days[i].answers.length;j++){
														if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
															if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
															 arr.push(data);
														}
													}
												}
												//console.log('arr',arr);
												var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
												$http({
													method:'POST',
													url:"php/customerInfo.php",
													data:detailddata,
													headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
												}).then(function successCallback(response){
													//console.log('res',response.data);
													alert(response.data['msg']);
													$state.go('dashboard.customer.new',{}, { reload: true });
												},function errorCallback(response) {
												})
												
										},function errorCallback(response) {
											console.log('err',response.data);
											alert(response.data['msg']);
										})
								}
							}else{
                  console.log('special multiple',$scope.example14model);
									var newpath='';
									var supplierData=$("#billdata").serialize();
									supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									$http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									}).then(function successCallback(response){
                    console.log('res first',response.data);
										for(var i=0;i<$scope.days.length;i++){
												for(var j=0;j<$scope.days[i].answers.length;j++){
													if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
														if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
														 arr.push(data);
													}
												}
											}
											//console.log('arr',arr);
											var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
											$http({
												method:'POST',
												url:"php/customerInfo.php",
												data:detailddata,
												headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											}).then(function successCallback(response){
												//console.log('res',response.data);
												alert(response.data['msg']);
												$state.go('dashboard.customer.new',{}, { reload: true });
											},function errorCallback(response) {
											})
											
									},function errorCallback(response) {
										console.log('err',response.data);
										alert(response.data['msg']);
									})
							}
						}
						if($scope.file==null && $scope.mulImage.length==0){
								var newpath='';
								var imageString='';
								var supplierData=$("#billdata").serialize();
								supplierData+= "&image="+newpath+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
								$http({
									 method:'POST',
									 url:"php/customerInfo.php",
									 data:supplierData,
									 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
								}).then(function successCallback(response){
                  console.log('res first',response.data);
									for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
										})
										
								},function errorCallback(response) {
									console.log('err',response.data);
									alert(response.data['msg']);
								})
						}
				}
				if($scope.imageData!=''){
					var imageString='';
				    var arrImage=[];
					
						for(var i=0;i<$scope.mulImage.length;i++){
							if($scope.mulImage[i]['image']!=null){
								var newmulpath='';
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
								//$scope.mulImage[i]['image'].name=newmulpath;
								$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
								arrImage.push({'image':$scope.mulImage[i]['image']});
								if(i==0){
									imageString=newmulpath;
								}else{
									imageString +=","+newmulpath;
								}
							}else{
								var newmulpath='';
								newmulpath=$scope.mulImage[i]['filename'];
								if(i==0){
									imageString=newmulpath;
								}else{
									imageString +=","+newmulpath;
								}
						   }
						}
						if(arrImage.length>0){
							//console.log('arr iamge',arrImage);
								$scope.upload=Upload.upload({
									 url: 'php/uploadAll.php',
									 method: 'POST',
									 file: arrImage
								}).success(function(data, status, headers, config) {
									 var supplierData=$("#billdata").serialize();
									 supplierData+= "&image="+$scope.imageData+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									 $http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									 }).then(function successCallback(response){
										 console.log('response',response.data);
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										//console.log('detail',detailddata);
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
											console.log('reso',response.data);
											alert(response.data['msg']);
										})
										
										
									 },function errorCallback(response) {
										 console.log('err1',response.data);
										 alert(response.data['msg']);
									 })
								}).error(function(data,status){
								})
						}else{
							var supplierData=$("#billdata").serialize();
							supplierData+= "&image="+$scope.imageData+"&action=add"+"&quadrant="+$scope.quadrant.value+"&mulimage="+imageString+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									 $http({
										 method:'POST',
										 url:"php/customerInfo.php",
										 data:supplierData,
										 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									 }).then(function successCallback(response){
										 console.log('response',response.data);
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
															}else{
																var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
															}
													 arr.push(data);
												}
											}
										}
										//console.log('arr',arr);
										var detailddata=$.param({'action':'subcatadd','tableData':arr,'member_id':response.data['member_id'],'quadrant':$scope.quadrant.value,'city':$scope.citycode.value});
										//console.log('detail',detailddata);
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											//console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.new',{}, { reload: true });
										},function errorCallback(response) {
											console.log('reso',response.data);
											alert(response.data['msg']);
										})
										
										
									 },function errorCallback(response) {
										 console.log('err1',response.data);
										 alert(response.data['msg']);
									 })
						}
					
				}
                 }
			}
		}else{
			if(billdata.bannerimage.$invalid){
				alert('Please add the valid image(i.e-.png or .jpeg)  of size upto 2 mb max');
				focusInputField.borderColor('imagefile1');
			}
			if(billdata.url.$invalid){
				alert('Please add a valid URL with http:// or https://');
				focusInputField.borderColor('weburl');
			}
            if(angular.isDefined($scope.mulImage)){
               // console.log('mul',$scope.mulImage,billdata);
                for(var i=0;i<$scope.mulImage.length;i++){
                        var name='upload_'+i;
                      //  console.log('mul',typeof(billdata.'upload_'+i),billdata);
                        if(billdata[name].$invalid){
                            alert('Please add the valid image(i.e-.png or .jpeg)  of size upto 2 mb max in image field'+(i+1));
                            return false;
                        }
                }
            }
            //if(billdata.)
		}
	}
	$scope.uploadFile = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
             var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded = function(e){
    $scope.$apply(function() {
        $scope.attchImage1=e.target.result;
		//$scope.showImage=true;
		//focusInputField.clearBorderColor('imagefile1');
    });
	}
	$scope.onFileSelect = function($files) {
		//console.log('file details',$files);
		 fileURL=$files;
		 $scope.imageData='';
		 //focusInputField.clearBorderColor('imagefile1');
	}
	/*$scope.listRestaurant=[{
		name:'Select Business Name',
		value:''
	}]*/
	//$scope.restname=angular.copy($scope.listRestaurant[0]);
   // $scope.parentrestname=angular.copy($scope.listRestaurant[0]);
    $scope.listRestaurant=[];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=rest",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
	$scope.displayResaurant=function(){
		$scope.displaycopy=false;
		$scope.restdisplay=true;
	}
   $scope.mulImage=[];
   $scope.mulImage.push({'image':null,'filename':''});
   $scope.addNewImageRow=function(mulImage){
       console.log('total image',mulImage.length);
	   mulImage.push({'image':null,'filename':''});
       console.log('end total image',mulImage.length);
	   
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	   mulImage.splice(index,1);
	   console.log('file',$scope.mulImage);
   }
   $scope.onFileSelect1 = function(index) {
	     $scope.mulImage[index]['filename']='';
			 /*var file = files;
			 var reader = new FileReader();
			 reader.onload = $scope.imageIsLoaded1(index); 
			 reader.readAsDataURL(file);*/
   }
  /* $scope.imageIsLoaded1=function(e,index){
	   console.log('e',e);
	   $scope.attachmultipleimage_index=e.target.result;
	   $scope.showImage[index]=true;
   }*/
    $scope.setSubcatag=function(index,childindex,catid,subcat_id,checked){
	 // console.log('hello',index,childindex,catid);
	  var catdata=$.param({'action':'subcat','cat_id':catid});
	  var subcategories=[];
	  $http({
		  method:'POST',
		  url:"php/customerInfo.php",
		  data:catdata,
		  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	  }).then(function successCallback(res){
		  angular.forEach(res.data,function(obj){
			  var data={'name':obj.subcat_name,'value':obj.subcat_id};
			  if(!$scope.listOfSubCategory[index]){
				  $scope.listOfSubCategory[index] = [];  
			  }
			  subcategories.push(data);
		  })
		  $scope.listOfSubCategory[index][childindex]=subcategories;
		 // console.log('sub',$scope.listOfSubCategory[index][childindex]);
		  $scope.days[index].answers[childindex].subcategory={'value':subcat_id};
		   if(checked==1){
		    $scope.days[index].answers[childindex].check=true;
			var thisAnswer=$scope.days[index].answers[childindex];
			$scope.selectedAnswers[index].chkbox.push(thisAnswer);
		  }
		console.log('subcat',$scope.days[index].answers);
	  },function errorCallback(response) {
	  });
	  
  }
   $scope.getQuadrantName=function(quadrant){
		$scope.listOfQuadrant=[];
		$scope.listOfQuadrant=[{
			name:'Select  Quadrant',
			value:''
		}]
		$scope.quadrant=$scope.listOfQuadrant[0];
		$http({
			method:'GET',
			url:"php/customerInfo.php?action=quad",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hello',response);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.quadrant,'value':obj.quad_id};
				$scope.listOfQuadrant.push(data);
				if(obj.quad_id==quadrant){
					$scope.quadrant.value=quadrant;
				}
			})
			
		},function errorCallback(response) {
		})
	}
   $scope.copyRestaurantDetails=function(){
	   //var detailsid=$.param({'member_id':$scope.restname.value});
	   $scope.mulImage=[];
       $scope.mulImage.push({'image':null,'filename':''});
	   if($scope.restname.value !=''){
           id=$scope.restname.value;
	   var memid=$.param({'action':'infoedit','member_id':$scope.restname.value});
		//console.log('member',memid);
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:memid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edit res',response.data);
            $window.sessionStorage.setItem('customerInfo',JSON.stringify(response.data));
            $window.sessionStorage.setItem('isCopyMode',true);
           // $window.sessionStorage.setItem('customerInfo',JSON.stringify(response.data));
            console.log('session', JSON.parse($window.sessionStorage.customerInfo),$window.sessionStorage.isCopyMode);
			$scope.name=response.data[0].rest_name;
			$scope.getQuadrantName(response.data[0].quadrant);
			//$scope.quadrant.value=response.data[0].quadrant;
			$scope.citycode.value=response.data[0].city;
			$scope.proviance=response.data[0].proviance;
			$scope.postal=response.data[0].postal;
			$scope.address=response.data[0].address;
      //$scope.special.value=response.data[0].special;
      var specArr=response.data[0].special.split(",");
      var stadata=$.param({'action':'chkStatusSpecial','spec_ids':response.data[0].special});
      $http({
            method:'POST',
            url:'php/customerInfo.php',
            data:stadata,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function successCallback(response){
            angular.forEach(response.data,function(obj){
              var data={'id':obj.special_id};
              $scope.example14model.push(data);
            })
      },function errorCallback(response) {
      })
      $timeout(function(){
        $scope.setCategoryValue();
      },2000)
			$scope.country=response.data[0].country;
			$scope.businessno=response.data[0].business_phone_no;
			$scope.conperson=response.data[0].person;
			$scope.mobno=response.data[0].mobile;
			$scope.email=response.data[0].email;
			$scope.url=response.data[0].url;
			$scope.status=response.data[0].status;
			$scope.latitude=response.data[0].latitude;
			$scope.longitude=response.data[0].longitude;
			$scope.premium_service=response.data[0].premium;
            $scope.parentrestname={};
            if(response.data[0].parent_member_id != 'null'){
                $scope.parentrestname.value=response.data[0].parent_member_id;
            }
			if(response.data[0].status==1 && response.data[0].image !=''){
				$scope.attchImage1="upload/"+response.data[0].image;
				$scope.imageData=response.data[0].image;
			}
			if(response.data[0].image ==''){
				$scope.attchImage1="img/no_image.jpg";
				$scope.imageData='';
			}
            if(response.data[0].parent_member_id != 'null'){
                $scope.parentrestname.value=response.data[0].parent_member_id;
            }
            
			var multiImage=response.data[0].multiple_image;
			var array=multiImage.split(",");
			//console.log('length',array.length);
			$scope.attach=[];
			$scope.dis=[];
			for(var i=0;i<array.length;i++){
				if(i==0){
					$scope.mulImage[i]['filename']=array[i];
				}
				if(i !=0){
				 $scope.mulImage.push({'image':null,'filename':array[i]});
				}
				 /*$scope.attach[i]=array[i];
				 $scope.dis[i]="upload/"+array[i];*/
				 //console.log('display',$scope.dis_i);
			}
			var memberid=$.param({'action':'detailedit','member_id':$scope.restname.value});
			$http({
				method:'POST',
				url:"php/customerInfo.php",
				data:memberid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('resdetails',response.data=='null');
				if(response.data !='null' && response.data !=''){
				var disparr=[];
				var counter = {};
				//var response=response.data;
                response.data.map(function(item) {
                   counter[item.day_id] = (counter[item.day_id] || 0) + 1;
                });
				console.log('counter',counter);
				var mondayarr=[];
				var tuesdayarr=[];
				var wedensdayarr=[];
				var thrusdayarr=[];
				var fridayarr=[];
				var saturdayarr=[];
				var sundayarr=[];
				//$scope.days[0].answers=[];
				for(var i=0;i<response.data.length;i++){
					if(response.data[i].day_id==1){
						mondayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==2){
						tuesdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==3){
						wedensdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==4){
						thrusdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==5){
						fridayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==6){
						saturdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==7){
						sundayarr.push(response.data[i]);
					}
				}
				console.log('day arr',thrusdayarr=='');
				if(mondayarr !=''){
				  $scope.days[0].answers=[];
				}
				if(tuesdayarr !=''){
				  $scope.days[1].answers=[];
				}
				if(wedensdayarr !=''){
				  $scope.days[2].answers=[];
				}
				if(thrusdayarr !=''){
				 $scope.days[3].answers=[];
				}
				if(fridayarr !=''){
				  $scope.days[4].answers=[];
				}
				if(saturdayarr !=''){
				  $scope.days[5].answers=[];
				}
				if(sundayarr !=''){
				 $scope.days[6].answers=[];
				}
				for(var i=0;i<mondayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(mondayarr[i].checked==1){
						$scope.days[0].answers.push({
								category:{'value':mondayarr[i].cat_id},
								subcategory: null,
								comment: mondayarr[i].comment,
						})
						console.log('checkbox',$scope.days[0].answers);
						$scope.setSubcatag(0,i,mondayarr[i].cat_id,mondayarr[i].subcat_id,1);
					}else{
						$scope.days[0].answers.push({
								category:{'value':mondayarr[i].cat_id},
								subcategory: null,
								comment: mondayarr[i].comment
						})
						$scope.setSubcatag(0,i,mondayarr[i].cat_id,mondayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<tuesdayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(tuesdayarr[i].checked==1){
						$scope.days[1].answers.push({
								category:{'value':tuesdayarr[i].cat_id},
								subcategory: null,
								comment: tuesdayarr[i].comment,
						})
						$scope.setSubcatag(1,i,tuesdayarr[i].cat_id,tuesdayarr[i].subcat_id,1);
					}else{
						$scope.days[1].answers.push({
								category:{'value':tuesdayarr[i].cat_id},
								subcategory: null,
								comment: tuesdayarr[i].comment
						})
						$scope.setSubcatag(1,i,tuesdayarr[i].cat_id,tuesdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<wedensdayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(wedensdayarr[i].checked==1){
						$scope.days[2].answers.push({
								category:{'value':wedensdayarr[i].cat_id},
								subcategory: null,
								comment: wedensdayarr[i].comment,
						})
						$scope.setSubcatag(2,i,wedensdayarr[i].cat_id,wedensdayarr[i].subcat_id,1);
					}else{
						$scope.days[2].answers.push({
								category:{'value':wedensdayarr[i].cat_id},
								subcategory: null,
								comment: wedensdayarr[i].comment
						})
						$scope.setSubcatag(2,i,wedensdayarr[i].cat_id,wedensdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<thrusdayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(thrusdayarr[i].checked==1){
						$scope.days[3].answers.push({
								category:{'value':thrusdayarr[i].cat_id},
								subcategory: null,
								comment: thrusdayarr[i].comment,
						})
						$scope.setSubcatag(3,i,thrusdayarr[i].cat_id,thrusdayarr[i].subcat_id,1);
					}else{
						$scope.days[3].answers.push({
								category:{'value':thrusdayarr[i].cat_id},
								subcategory: null,
								comment: thrusdayarr[i].comment,
						})
						$scope.setSubcatag(3,i,thrusdayarr[i].cat_id,thrusdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<fridayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(fridayarr[i].checked==1){
						$scope.days[4].answers.push({
								category:{'value':fridayarr[i].cat_id},
								subcategory: null,
								comment: fridayarr[i].comment,
						})
						$scope.setSubcatag(4,i,fridayarr[i].cat_id,fridayarr[i].subcat_id,1);
					}else{
						$scope.days[4].answers.push({
								category:{'value':fridayarr[i].cat_id},
								subcategory: null,
								comment: fridayarr[i].comment,
						})
						$scope.setSubcatag(4,i,fridayarr[i].cat_id,fridayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<saturdayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(saturdayarr[i].checked==1){
						$scope.days[5].answers.push({
								category:{'value':saturdayarr[i].cat_id},
								subcategory: null,
								comment: saturdayarr[i].comment,
						})
						$scope.setSubcatag(5,i,saturdayarr[i].cat_id,saturdayarr[i].subcat_id,1);
					}else{
						$scope.days[5].answers.push({
								category:{'value':saturdayarr[i].cat_id},
								subcategory: null,
								comment: saturdayarr[i].comment
						})
						$scope.setSubcatag(5,i,saturdayarr[i].cat_id,saturdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<sundayarr.length;i++){
					//console.log('i',i);
					$scope.listOfSubCategory =[]; 
					if(sundayarr[i].checked==1){
						$scope.days[6].answers.push({
								category:{'value':sundayarr[i].cat_id},
								subcategory: null,
								comment: sundayarr[i].comment,
						})
						$scope.setSubcatag(6,i,sundayarr[i].cat_id,sundayarr[i].subcat_id,1);
					}else{
						$scope.days[6].answers.push({
								category:{'value':sundayarr[i].cat_id},
								subcategory: null,
								comment: sundayarr[i].comment
						})
						$scope.setSubcatag(6,i,sundayarr[i].cat_id,sundayarr[i].subcat_id,0);
					}
				}
				/*for(var i=0;i<response.data.length;i++){
					var k='';
					if(response.data[i].day_id==1){
						k=0;
					}else if(response.data[i].day_id==2){
						k=1;
					}else if(response.data[i].day_id==3){
						k=2;
					}else if(response.data[i].day_id==4){
						k=3;
					}else if(response.data[i].day_id==5){
						k=4;
					}else if(response.data[i].day_id==6){
						k=5;
					}else{
						k=6;
					}
					
							if(response.data[i].position==1){
								var cat="catagory"+k;
								var subcat="subcatagory"+k;
								//console.log('cat',cat,subcat);
								document.getElementById(cat).value =response.data[i].cat_id;
								var subcatid=response.data[i].subcat_id;
								$scope.setSubCatagory(response.data[i].cat_id,k,subcat,subcatid);
								var commnt="comment"+k;
								
								document.getElementById(commnt).value =response.data[i].comment;
							
							}
							if(response.data[i].position==2){
								var cat1="cata"+k;
								var subcat1="subcata"+k;
								var comment1="comm"+k;
								
								document.getElementById(cat1).value =response.data[i].cat_id;
								var subcatid=response.data[i].subcat_id;
								$scope.setSubCatagory1(response.data[i].cat_id,k,subcat1,subcatid);
								document.getElementById(comment1).value =response.data[i].comment;
								
							}
				}*/
				}
			},function errorCallback(response) {
			})
		},function errorCallback(response) {
		})
	   }
   }
  $scope.answerIsSelected = function(dayIdx, answerIdx) {
    var thisAnswer = $scope.days[dayIdx].answers[answerIdx];
   // return $scope.selectedAnswers.indexOf(thisAnswer) > -1;
   return $scope.selectedAnswers[dayIdx].chkbox.indexOf(thisAnswer) > -1;
  };
  
  $scope.toggleAnswerSelected = function(dayIdx, answerIdx) {
    var thisAnswer = $scope.days[dayIdx].answers[answerIdx];
		if ($scope.answerIsSelected(dayIdx, answerIdx)) {
		 // $scope.selectedAnswers.splice($scope.selectedAnswers.indexOf(thisAnswer), 1);
		 $scope.selectedAnswers[dayIdx].chkbox.splice($scope.selectedAnswers.indexOf(thisAnswer), 1);
		}else{
		   $scope.selectedAnswers[dayIdx].chkbox.push(thisAnswer);
		}
  };
  
  $scope.isDisabled = function(dayIdx, answerIdx) {
    //return !$scope.answerIsSelected(dayIdx, answerIdx) && $scope.selectedAnswers.length === 2;
	return !$scope.answerIsSelected(dayIdx, answerIdx) &&$scope.selectedAnswers[dayIdx].chkbox.length === 2;
  };
  $scope.getLatitude=function(address,callback){
	   geocoder = new google.maps.Geocoder();
	   if (geocoder) {
		   geocoder.geocode({
			   'address': address
		   },function (results, status) {
			   if (status == google.maps.GeocoderStatus.OK) {
				    callback(results[0]);
			   }
		   })
	   }
  }
  $scope.getLatValue=function(){
      var cityName='';
	  if($scope.address==null || $scope.address==''){
		  alert('Please enter the address');
	  }else if($scope.citycode.value==null || $scope.citycode.value==''){
          alert('Please select the city');
      }else if($scope.postal==null || $scope.postal==''){
          alert('Please enter the postal code');
      }else{
          var citydata=$.param({'action':'getcity','city':$scope.citycode.value});
          $http({
              method:'POST',
              url:'php/customerInfo.php',
              data:citydata,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).then(function successCallback(response){
              // console.log('city',response.data);
              cityName=response.data[0].city_name;
          },function errorCallback(response) {
          })
           $timeout(function(){
              var address=$scope.address+','+cityName+','+$scope.postal;
              console.log('address',address);
              $scope.getLatitude(address,function(result){
                 // console.log('result',result.geometry.location.lat());
                  $timeout(function(){
                    $scope.latitude=result.geometry.location.lat();
                    $scope.longitude=result.geometry.location.lng();
                  },1000);
              });
           },1000);
	  }
  }
  $scope.getLongValue=function(){
      var cityName='';
	 if($scope.address==null || $scope.address==''){
		  alert('Please enter the address');
	  }else if($scope.citycode.value==null || $scope.citycode.value==''){
          alert('Please select the city');
      }else if($scope.postal==null || $scope.postal==''){
          alert('Please enter the postal code');
      }else{
		  var citydata=$.param({'action':'getcity','city':$scope.citycode.value});
          $http({
              method:'POST',
              url:'php/customerInfo.php',
              data:citydata,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).then(function successCallback(response){
              cityName=response.data[0].city_name;
          },function errorCallback(response) {
          })
		 $timeout(function(){
              var address=$scope.address+','+cityName+','+$scope.postal;
              console.log('address',address);
              $scope.getLatitude(address,function(result){
                 // console.log('result',result.geometry.location.lat());
                  $timeout(function(){
                    $scope.latitude=result.geometry.location.lat();
                    $scope.longitude=result.geometry.location.lng();
                  },1000);
              });
           },1000);
	  } 
  }
  $scope.getMap=function(){
       var base_url = "index.html#" 
       var url = $state.href('map', {latitude:$scope.latitude,longitude:$scope.longitude,'title':$scope.address});
       $window.open(base_url+url,'_blank'); 
  }
  
});
customerNew.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
customerNew.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});