<?php
require_once '../../include/dbconfig.php'; 
if(isset($_REQUEST["action"]) && $_REQUEST["action"] !=""){
	if($_REQUEST["action"]=="disp"){
		$sql="select * from db_specials order by special_id desc";
		$query=mysqli_query($connect,$sql);
		while($row=mysqli_fetch_array($query)){
			$data[]=$row;
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="add"){
		$specialname=$_POST['specialname'];
		$type=$_POST['type'];
		$sqlduplicate=mysqli_query($connect,"select * from db_specials where type='".$type."'");
		if(mysqli_num_rows($sqlduplicate) > 0){
			$row=mysqli_fetch_array($sqlduplicate);
			$type=$row['type'];
			$result=array();
			header("HTTP/1.0 401 Unauthorized");
			$result['msg']=$type." is already exist.Please try another one";
			print json_encode($result);
			return;
		}
		$duplicate=mysqli_query($connect,"select * from db_specials where special_name='".$specialname."'");
		if(mysqli_num_rows($duplicate) > 0){
			$row=mysqli_fetch_array($duplicate);
			$special=$row['special_name'];
			$result=array();
			header("HTTP/1.0 401 Unauthorized");
			$result['msg']=$special." is already exist.Please try another one";
			print json_encode($result);
			return;
		}
		{
			$sql=mysqli_query($connect,'INSERT INTO db_specials (special_name,type) values ("'.$specialname.'","'.$type.'")');
			if($sql){
				$result['msg']="Successfully added";
			    print json_encode($result);
			}
		}
	}
	if($_REQUEST["action"]=="edit"){
		$special_id=$_POST['special_id'];
		$sql="select * from db_specials where special_id='".$special_id."'";
		$query=mysqli_query($connect,$sql);
		while($row=mysqli_fetch_array($query)){
			$data[]=$row;
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="update"){
		$special_id=$_POST['special_id'];
		$specialname=$_POST['specialname'];
		$type=$_POST['type'];
		$sqlduplicate=mysqli_query($connect,"select * from db_specials where type='".$type."' and special_id !='".$special_id."'");
		if(mysqli_num_rows($sqlduplicate) > 0){
			$row=mysqli_fetch_array($sqlduplicate);
			$type=$row['type'];
			$result=array();
			header("HTTP/1.0 401 Unauthorized");
			$result['msg']=$type." is already exist.Please try another one";
			print json_encode($result);
			return;
		}
		$duplicate=mysqli_query($connect,"select * from db_specials where special_name='".$specialname."' and special_id !='".$special_id."'");
		if(mysqli_num_rows($duplicate) > 0){
			$row=mysqli_fetch_array($duplicate);
			$special=$row['special_name'];
			$result=array();
			header("HTTP/1.0 401 Unauthorized");
			$result['msg']=$special." is already exist.Please try another one";
			print json_encode($result);
			return;
		}
		{
			$qry = "UPDATE db_specials SET special_name='$specialname',type='$type' WHERE special_id='$special_id'";
			$query=mysqli_query($connect,$qry);
			if($query){
				$result['msg']="Successfully updated";
				print json_encode($result);
			}
		}
		
	}
	if($_REQUEST["action"]=="delete"){
		$special_id=$_POST['special_id'];
        $sqlchk=mysqli_query($connect,"select * from db_category where special='".$special_id."'");
        if(mysqli_num_rows($sqlchk) > 0){
            $sql=mysqli_query("select * from db_specials where special_id='".$special_id."'");
            $row=mysqli_fetch_array($sql);
            $special_name=$row['special_name'];
            $result=array();
			header("HTTP/1.0 401 Unauthorized");
			$result['msg']= "You can not delete this.because".$special_name." is already used in category section";
			print json_encode($result);
			return;
        }
        $sqlchk=mysqli_query($connect,"select * from db_restaurant_basic where special='".$special_id."'");
        if(mysqli_num_rows($sqlchk) > 0){
            $sql=mysqli_query("select * from db_specials where special_id='".$special_id."'");
            $row=mysqli_fetch_array($sql);
            $special_name=$row['special_name'];
            $result=array();
			header("HTTP/1.0 401 Unauthorized");
			$result['msg']= "You can not delete this.because".$special_name." is already used in business information section";
			print json_encode($result);
			return;
        }
		$delsql=mysqli_query($connect,"delete from db_specials where special_id='".$special_id."' ");
		if($delsql){
			$result['msg']="Deleted successfully";
			print json_encode($result);
		}
	}
    if($_REQUEST["action"]=="inactive"){
        $special_id=$_POST['special_id'];
        $status=$_POST['status'];
      //  echo  $status;exit;
        if($status==1){
            $upsucat=mysqli_query($connect,"UPDATE db_specials SET status=0 WHERE special_id='$special_id'");
             $result['msg']="Special inactivated successfully";
        }
        if($status==0){
            $upsucat=mysqli_query($connect,"UPDATE db_specials SET status=1 WHERE special_id='$special_id'"); 
             $result['msg']="Special activated successfully";
        }
         print json_encode($result);
    }
}
?>