<?php
require_once '../include/dbconfig.php';
if(isset($_REQUEST["action"]) && $_REQUEST["action"] !=""){
	if($_REQUEST["action"]=="disp"){
		$sql="select s.summery_id,s.member_id,s.hit_type,sum(if(s.hit_type=1,1,0)) as page_hit,sum(if(s.hit_type=2,1,0))as map_hit,sum(if(s.hit_type=3,1,0)) as gallery_hit,sum(if(s.hit_type=4,1,0)) as phone_hit,sum(if(s.hit_type=5,1,0)) as web_hit,count(1) as total,s.device_type,s.counter,s.date,r.rest_name,h.name,h.type";
		$sql.=" from db_analytics_summery as s";
		$sql.=" left join db_hit_type as h on s.hit_type=h.type";
		$sql.=" left join db_restaurant_basic as r on s.member_id=r.member_id";
		$sql.=" group by s.member_id order by s.summery_id desc";
		$res=mysqli_query($connect,$sql);
		while($row=mysqli_fetch_array($res)){
			$data[]=$row;
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="detail"){
		//$sql="select s.summery_id,s.member_id,sum(if(s.device_type=1,1,0)) as IOS,sum(if(s.device_type(2,1,0)) as Android,s.date,r.rest_name,d.resultant_counter";
		//$sql.=" from db_analytics_summery as s";
		//$sql.=" left join db_restaurant_basic as r on s.member_id=r.member_id";
		//$sql.=" left join db_analytics as d on s.member_id=d.member_id";
		//$sql.=" group by s.member_id order by s.summery_id desc";
		$sql="select s.summery_id,s.member_id,sum(if(s.device_type=1,1,0)) as IOS,sum(if(s.device_type=2,1,0)) as Android,r.rest_name,d.resultant_counter,d.date";
		$sql.=" from db_analytics_summery as s";
		$sql.=" left join db_analytics as d on s.member_id=d.member_id";
		$sql.=" left join db_restaurant_basic as r on s.member_id=r.member_id";
		$sql.=" group by s.member_id order by s.summery_id desc";
		//echo $sql;exit;
		$res=mysqli_query($connect,$sql);
		while($row=mysqli_fetch_array($res)){
            if($row['rest_name'] !=null || $row['rest_name'] !=''){
                $data[]=$row;
            }
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="search"){
		$summary_id=$_POST['summary'];
		$len=count($summary_id);
		for($i=0;$i<$len;$i++){
			$sum_id=$summary_id[$i]['summary_id'];
			$sql="select s.summery_id,s.member_id,sum(if(s.device_type=1,1,0)) as IOS,sum(if(s.device_type=2,1,0)) as Android,r.rest_name,d.resultant_counter,d.date";
		    $sql.=" from db_analytics_summery as s";
		    $sql.=" left join db_analytics as d on s.member_id=d.member_id";
		    $sql.=" left join db_restaurant_basic as r on s.member_id=r.member_id";
		    $sql.=" where s.member_id='".$sum_id."'";
			$res=mysqli_query($connect,$sql);
			while($row=mysqli_fetch_array($res)){
			   $data[]=$row;
		    }
		}
		 print json_encode($data);
	}
	if($_REQUEST["action"]=="bussumdisp"){
		$sql="select s.summery_id,s.member_id,s.hit_type,sum(if(s.hit_type=1,1,0)) as page_hit,sum(if(s.hit_type=2,1,0))as map_hit,sum(if(s.hit_type=3,1,0)) as gallery_hit,sum(if(s.hit_type=4,1,0)) as phone_hit,sum(if(s.hit_type=5,1,0)) as web_hit,count(1) as total,s.device_type,s.counter,s.date,r.rest_name,h.name,h.type";
		$sql.=" from db_analytics_summery as s";
		$sql.=" left join db_hit_type as h on s.hit_type=h.type";
		$sql.=" left join db_restaurant_basic as r on s.member_id=r.member_id";
		$sql.=" where s.member_id='".$_SESSION["member_id"]."' group by s.member_id order by s.summery_id desc";
		$res=mysqli_query($connect,$sql);
		if(mysqli_num_rows($res) > 0){
			while($row=mysqli_fetch_array($res)){
				$data[]=$row;
			}
		}else{
			$data=array();
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="businessdetail"){
		$sql="select s.summery_id,s.member_id,sum(if(s.device_type=1,1,0)) as IOS,sum(if(s.device_type=2,1,0)) as Android,r.rest_name,d.resultant_counter,d.date";
		$sql.=" from db_analytics_summery as s";
		$sql.=" left join db_analytics as d on s.member_id=d.member_id";
		$sql.=" left join db_restaurant_basic as r on s.member_id=r.member_id";
		$sql.=" where s.member_id='".$_SESSION["member_id"]."' group by s.member_id order by s.summery_id desc";
		//echo $sql;exit;
		$res=mysqli_query($connect,$sql);
		if(mysqli_num_rows($res) >0){
			while($row=mysqli_fetch_array($res)){
				$data[]=$row;
			}
		}else{
			$data=array();
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="subsumdisp"){
		$sql="select s.subcat_summary_id,s.subcat_id,s.hit_type,sum(if(s.hit_type=6,1,0)) as total_hit,s.device_type,s.counter,s.date,r.subcat_name,h.name,h.type";
		$sql.=" from db_subcat_analytics_summary as s";
		$sql.=" left join db_hit_type as h on s.hit_type=h.type";
		$sql.=" left join db_subcategory as r on s.subcat_id=r.subcat_id";
		$sql.=" group by s.subcat_id order by s.subcat_summary_id desc";
		$res=mysqli_query($connect,$sql);
		while($row=mysqli_fetch_array($res)){
            if($row['subcat_name'] != null){
                $data[]=$row;
            }
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="subcatdetail"){
		$sql="select s.subcat_summary_id,s.subcat_id,sum(if(s.device_type=1,1,0)) as IOS,sum(if(s.device_type=2,1,0)) as Android,r.subcat_name,d.resultant_counter,d.date";
		$sql.=" from db_subcat_analytics_summary as s";
		$sql.=" left join db_subcat_analytics as d on s.subcat_id=d.subcat_id";
		$sql.=" left join db_subcategory as r on s.subcat_id=r.subcat_id";
		$sql.=" group by s.subcat_id order by s.subcat_summary_id desc";
		$res=mysqli_query($connect,$sql);
		while($row=mysqli_fetch_array($res)){
            if($row['subcat_name'] != null){
			    $data[]=$row;
            }
		}
		print json_encode($data);
	}
	if($_REQUEST["action"]=="Subcatsearch"){
		$summary_id=$_POST['summary'];
		$len=count($summary_id);
		for($i=0;$i<$len;$i++){
			$sum_id=$summary_id[$i]['summary_id'];
			$sql="select s.subcat_summary_id,s.subcat_id,sum(if(s.device_type=1,1,0)) as IOS,sum(if(s.device_type=2,1,0)) as Android,r.subcat_name,d.resultant_counter,d.date";
		    $sql.=" from db_subcat_analytics_summary as s";
		    $sql.=" left join db_subcat_analytics as d on s.subcat_id=d.subcat_id";
		    $sql.=" left join db_subcategory as r on s.subcat_id=r.subcat_id";
		    $sql.=" where s.subcat_id='".$sum_id."'";
			$res=mysqli_query($connect,$sql);
			while($row=mysqli_fetch_array($res)){
			   $data[]=$row;
		    }
		}
		 print json_encode($data);
	}
}
?>