<?php
require_once '../../include/dbconfig.php'; 

$data = array();


$result = mysqli_query($connect, "SELECT * FROM db_profile");
$cnt = mysqli_num_rows($result);
$data["no_college"] = $cnt;

$result = mysqli_query($connect, "SELECT * FROM db_stream");
$cnt = mysqli_num_rows($result);
$data["no_stream"] = $cnt;

$result = mysqli_query($connect, "SELECT * FROM db_department");
$cnt = mysqli_num_rows($result);
$data["no_department"] = $cnt;

$result = mysqli_query($connect, "SELECT * FROM db_course");
$cnt = mysqli_num_rows($result);
$data["no_course"] = $cnt;

$result = mysqli_query($connect, "SELECT * FROM db_user");
$cnt = mysqli_num_rows($result);
$data["no_user"] = $cnt - 1 ;

print json_encode($data);
?>